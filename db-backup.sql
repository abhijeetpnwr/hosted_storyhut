-- MySQL dump 10.13  Distrib 5.5.54, for debian-linux-gnu (x86_64)
--
-- Host: abhijeetpnwr.mysql.pythonanywhere-services.com    Database: abhijeetpnwr$storyhut
-- ------------------------------------------------------
-- Server version	5.6.27-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `auth_group`
--

DROP TABLE IF EXISTS `auth_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group`
--

LOCK TABLES `auth_group` WRITE;
/*!40000 ALTER TABLE `auth_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_group_permissions`
--

DROP TABLE IF EXISTS `auth_group_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_group_permissions_group_id_0cd325b0_uniq` (`group_id`,`permission_id`),
  KEY `auth_group_permissi_permission_id_84c5c92e_fk_auth_permission_id` (`permission_id`),
  CONSTRAINT `auth_group_permissi_permission_id_84c5c92e_fk_auth_permission_id` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `auth_group_permissions_group_id_b120cbf9_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group_permissions`
--

LOCK TABLES `auth_group_permissions` WRITE;
/*!40000 ALTER TABLE `auth_group_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_permission`
--

DROP TABLE IF EXISTS `auth_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_permission_content_type_id_01ab375a_uniq` (`content_type_id`,`codename`),
  CONSTRAINT `auth_permissi_content_type_id_2f476e4b_fk_django_content_type_id` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_permission`
--

LOCK TABLES `auth_permission` WRITE;
/*!40000 ALTER TABLE `auth_permission` DISABLE KEYS */;
INSERT INTO `auth_permission` VALUES (1,'Can add story',1,'add_story'),(2,'Can change story',1,'change_story'),(3,'Can delete story',1,'delete_story'),(4,'Can add log entry',2,'add_logentry'),(5,'Can change log entry',2,'change_logentry'),(6,'Can delete log entry',2,'delete_logentry'),(7,'Can add user',3,'add_user'),(8,'Can change user',3,'change_user'),(9,'Can delete user',3,'delete_user'),(10,'Can add permission',4,'add_permission'),(11,'Can change permission',4,'change_permission'),(12,'Can delete permission',4,'delete_permission'),(13,'Can add group',5,'add_group'),(14,'Can change group',5,'change_group'),(15,'Can delete group',5,'delete_group'),(16,'Can add content type',6,'add_contenttype'),(17,'Can change content type',6,'change_contenttype'),(18,'Can delete content type',6,'delete_contenttype'),(19,'Can add session',7,'add_session'),(20,'Can change session',7,'change_session'),(21,'Can delete session',7,'delete_session');
/*!40000 ALTER TABLE `auth_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user`
--

DROP TABLE IF EXISTS `auth_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(128) NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(150) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(30) NOT NULL,
  `email` varchar(254) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user`
--

LOCK TABLES `auth_user` WRITE;
/*!40000 ALTER TABLE `auth_user` DISABLE KEYS */;
INSERT INTO `auth_user` VALUES (1,'pbkdf2_sha256$30000$S1Hegsm1tUSs$q2vZeojTfmdPRexBAgPoBWE0u+dWox8IAKyN9P8D6sw=','2017-06-09 20:46:48',1,'abhijeet','','','abhijeetpnwr@gmail.com',1,1,'2017-05-04 03:39:42');
/*!40000 ALTER TABLE `auth_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_groups`
--

DROP TABLE IF EXISTS `auth_user_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_user_groups_user_id_94350c0c_uniq` (`user_id`,`group_id`),
  KEY `auth_user_groups_group_id_97559544_fk_auth_group_id` (`group_id`),
  CONSTRAINT `auth_user_groups_group_id_97559544_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  CONSTRAINT `auth_user_groups_user_id_6a12ed8b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_groups`
--

LOCK TABLES `auth_user_groups` WRITE;
/*!40000 ALTER TABLE `auth_user_groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_user_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_user_permissions`
--

DROP TABLE IF EXISTS `auth_user_user_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user_user_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_user_user_permissions_user_id_14a6b632_uniq` (`user_id`,`permission_id`),
  KEY `auth_user_user_perm_permission_id_1fbb5f2c_fk_auth_permission_id` (`permission_id`),
  CONSTRAINT `auth_user_user_perm_permission_id_1fbb5f2c_fk_auth_permission_id` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_user_permissions`
--

LOCK TABLES `auth_user_user_permissions` WRITE;
/*!40000 ALTER TABLE `auth_user_user_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_user_user_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_admin_log`
--

DROP TABLE IF EXISTS `django_admin_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action_time` datetime NOT NULL,
  `object_id` longtext,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) unsigned NOT NULL,
  `change_message` longtext NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `django_admin__content_type_id_c4bce8eb_fk_django_content_type_id` (`content_type_id`),
  KEY `django_admin_log_user_id_c564eba6_fk_auth_user_id` (`user_id`),
  CONSTRAINT `django_admin__content_type_id_c4bce8eb_fk_django_content_type_id` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`),
  CONSTRAINT `django_admin_log_user_id_c564eba6_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=75 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_admin_log`
--

LOCK TABLES `django_admin_log` WRITE;
/*!40000 ALTER TABLE `django_admin_log` DISABLE KEYS */;
INSERT INTO `django_admin_log` VALUES (1,'2017-05-04 04:51:06','1','Story object',1,'[{\"added\": {}}]',1,1),(2,'2017-05-04 05:00:41','2','Story object',1,'[{\"added\": {}}]',1,1),(3,'2017-05-04 05:04:39','3','Story object',1,'[{\"added\": {}}]',1,1),(4,'2017-05-04 05:07:00','4','Story object',1,'[{\"added\": {}}]',1,1),(5,'2017-05-04 17:05:34','4','Story object',2,'[{\"changed\": {\"fields\": [\"cover_loc\"]}}]',1,1),(6,'2017-05-04 17:06:50','3','Story object',2,'[{\"changed\": {\"fields\": [\"cover_loc\"]}}]',1,1),(7,'2017-05-04 17:06:59','2','Story object',2,'[{\"changed\": {\"fields\": [\"cover_loc\"]}}]',1,1),(8,'2017-05-04 17:07:06','1','Story object',2,'[{\"changed\": {\"fields\": [\"cover_loc\"]}}]',1,1),(9,'2017-05-06 10:43:45','3','Story object',2,'[{\"changed\": {\"fields\": [\"story_title\", \"brief\"]}}]',1,1),(10,'2017-05-06 11:08:50','4','Story object',2,'[{\"changed\": {\"fields\": [\"cover_loc\", \"brief\"]}}]',1,1),(11,'2017-05-06 11:09:00','3','Story object',2,'[{\"changed\": {\"fields\": [\"cover_loc\"]}}]',1,1),(12,'2017-05-06 11:09:08','2','Story object',2,'[{\"changed\": {\"fields\": [\"cover_loc\", \"brief\"]}}]',1,1),(13,'2017-05-06 11:09:17','1','Story object',2,'[{\"changed\": {\"fields\": [\"cover_loc\", \"brief\"]}}]',1,1),(14,'2017-05-06 11:12:34','4','Story object',2,'[]',1,1),(15,'2017-05-06 11:12:44','3','Story object',2,'[]',1,1),(16,'2017-05-06 11:12:54','2','Story object',2,'[]',1,1),(17,'2017-05-06 11:13:05','1','Story object',2,'[]',1,1),(18,'2017-05-06 11:15:47','4','Story object',2,'[{\"changed\": {\"fields\": [\"cover_loc\"]}}]',1,1),(19,'2017-05-06 11:16:08','3','Story object',2,'[{\"changed\": {\"fields\": [\"cover_loc\"]}}]',1,1),(20,'2017-05-06 11:16:19','2','Story object',2,'[{\"changed\": {\"fields\": [\"cover_loc\"]}}]',1,1),(21,'2017-05-06 11:16:29','1','Story object',2,'[{\"changed\": {\"fields\": [\"cover_loc\"]}}]',1,1),(22,'2017-05-06 11:32:25','5','Story object',1,'[{\"added\": {}}]',1,1),(23,'2017-05-06 11:35:55','6','Story object',1,'[{\"added\": {}}]',1,1),(24,'2017-05-06 11:45:32','7','Story object',1,'[{\"added\": {}}]',1,1),(25,'2017-05-06 11:46:22','7','Story object',2,'[{\"changed\": {\"fields\": [\"writer\"]}}]',1,1),(26,'2017-05-06 11:48:59','8','Story object',1,'[{\"added\": {}}]',1,1),(27,'2017-05-06 11:52:28','9','Story object',1,'[{\"added\": {}}]',1,1),(28,'2017-05-06 11:54:49','10','Story object',1,'[{\"added\": {}}]',1,1),(29,'2017-05-06 11:58:06','11','Story object',1,'[{\"added\": {}}]',1,1),(30,'2017-05-06 12:17:46','12','Story object',1,'[{\"added\": {}}]',1,1),(31,'2017-05-06 12:20:13','13','Story object',1,'[{\"added\": {}}]',1,1),(32,'2017-05-06 12:22:13','14','Story object',1,'[{\"added\": {}}]',1,1),(33,'2017-05-06 12:24:29','15','Story object',1,'[{\"added\": {}}]',1,1),(34,'2017-05-06 12:32:16','16','Story object',1,'[{\"added\": {}}]',1,1),(35,'2017-05-06 15:15:35','17','Story object',1,'[{\"added\": {}}]',1,1),(36,'2017-05-06 15:16:47','18','Story object',1,'[{\"added\": {}}]',1,1),(37,'2017-05-06 15:18:18','19','Story object',1,'[{\"added\": {}}]',1,1),(38,'2017-05-06 15:19:42','20','Story object',1,'[{\"added\": {}}]',1,1),(39,'2017-05-06 15:21:24','21','Story object',1,'[{\"added\": {}}]',1,1),(40,'2017-05-06 15:23:38','22','Story object',1,'[{\"added\": {}}]',1,1),(41,'2017-05-06 15:25:17','23','Story object',1,'[{\"added\": {}}]',1,1),(42,'2017-05-06 15:26:51','24','Story object',1,'[{\"added\": {}}]',1,1),(43,'2017-05-06 15:29:03','25','Story object',1,'[{\"added\": {}}]',1,1),(44,'2017-05-06 15:30:39','26','Story object',1,'[{\"added\": {}}]',1,1),(45,'2017-05-06 15:33:03','27','Story object',1,'[{\"added\": {}}]',1,1),(46,'2017-05-06 15:34:55','28','Story object',1,'[{\"added\": {}}]',1,1),(47,'2017-05-06 15:37:27','29','Story object',1,'[{\"added\": {}}]',1,1),(48,'2017-05-06 15:38:53','30','Story object',1,'[{\"added\": {}}]',1,1),(49,'2017-05-06 15:40:12','31','Story object',1,'[{\"added\": {}}]',1,1),(50,'2017-06-09 20:54:30','32','Story object',1,'[{\"added\": {}}]',1,1),(51,'2017-06-09 21:01:18','32','Story object',2,'[{\"changed\": {\"fields\": [\"cover_loc\"]}}]',1,1),(52,'2017-06-09 21:13:32','33','Story object',1,'[{\"added\": {}}]',1,1),(53,'2017-06-09 21:20:19','34','Story object',1,'[{\"added\": {}}]',1,1),(54,'2017-06-10 03:26:54','34','Story object',2,'[{\"changed\": {\"fields\": [\"cover_loc\"]}}]',1,1),(55,'2017-06-10 03:38:59','35','Story object',1,'[{\"added\": {}}]',1,1),(56,'2017-06-10 03:57:53','34','Story object',2,'[{\"changed\": {\"fields\": [\"story_title\"]}}]',1,1),(57,'2017-06-10 04:02:39','34','Story object',2,'[{\"changed\": {\"fields\": [\"storyfile\"]}}]',1,1),(58,'2017-06-10 04:06:21','35','Story object',2,'[{\"changed\": {\"fields\": [\"story_title\", \"brief\"]}}]',1,1),(59,'2017-06-10 04:07:36','35','Story object',2,'[{\"changed\": {\"fields\": [\"brief\"]}}]',1,1),(60,'2017-06-10 04:13:46','36','Story object',1,'[{\"added\": {}}]',1,1),(61,'2017-06-10 04:15:21','33','Story object',2,'[{\"changed\": {\"fields\": [\"cover_loc\"]}}]',1,1),(62,'2017-06-10 04:23:34','37','Story object',1,'[{\"added\": {}}]',1,1),(63,'2017-06-10 04:31:08','38','Story object',1,'[{\"added\": {}}]',1,1),(64,'2017-06-10 04:33:56','37','Story object',2,'[{\"changed\": {\"fields\": [\"brief\"]}}]',1,1),(65,'2017-06-10 04:34:44','37','Story object',2,'[{\"changed\": {\"fields\": [\"brief\"]}}]',1,1),(66,'2017-06-10 04:44:52','39','Story object',1,'[{\"added\": {}}]',1,1),(67,'2017-06-10 04:46:47','39','Story object',2,'[{\"changed\": {\"fields\": [\"brief\"]}}]',1,1),(68,'2017-06-10 04:52:29','40','Story object',1,'[{\"added\": {}}]',1,1),(69,'2017-06-10 04:59:41','41','Story object',1,'[{\"added\": {}}]',1,1),(70,'2017-06-10 05:04:56','42','Story object',1,'[{\"added\": {}}]',1,1),(71,'2017-06-10 05:10:56','43','Story object',1,'[{\"added\": {}}]',1,1),(72,'2017-06-10 05:34:12','44','Story object',1,'[{\"added\": {}}]',1,1),(73,'2017-06-10 05:37:22','44','Story object',2,'[{\"changed\": {\"fields\": [\"cover_loc\"]}}]',1,1),(74,'2017-06-10 05:48:51','45','Story object',1,'[{\"added\": {}}]',1,1);
/*!40000 ALTER TABLE `django_admin_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_content_type`
--

DROP TABLE IF EXISTS `django_content_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `django_content_type_app_label_76bd3d3b_uniq` (`app_label`,`model`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_content_type`
--

LOCK TABLES `django_content_type` WRITE;
/*!40000 ALTER TABLE `django_content_type` DISABLE KEYS */;
INSERT INTO `django_content_type` VALUES (2,'admin','logentry'),(5,'auth','group'),(4,'auth','permission'),(3,'auth','user'),(6,'contenttypes','contenttype'),(1,'personal','story'),(7,'sessions','session');
/*!40000 ALTER TABLE `django_content_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_migrations`
--

DROP TABLE IF EXISTS `django_migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_migrations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `applied` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_migrations`
--

LOCK TABLES `django_migrations` WRITE;
/*!40000 ALTER TABLE `django_migrations` DISABLE KEYS */;
INSERT INTO `django_migrations` VALUES (1,'contenttypes','0001_initial','2017-05-04 03:19:11'),(2,'auth','0001_initial','2017-05-04 03:19:13'),(3,'admin','0001_initial','2017-05-04 03:19:13'),(4,'admin','0002_logentry_remove_auto_add','2017-05-04 03:19:13'),(5,'contenttypes','0002_remove_content_type_name','2017-05-04 03:19:14'),(6,'auth','0002_alter_permission_name_max_length','2017-05-04 03:19:14'),(7,'auth','0003_alter_user_email_max_length','2017-05-04 03:19:14'),(8,'auth','0004_alter_user_username_opts','2017-05-04 03:19:14'),(9,'auth','0005_alter_user_last_login_null','2017-05-04 03:19:14'),(10,'auth','0006_require_contenttypes_0002','2017-05-04 03:19:14'),(11,'auth','0007_alter_validators_add_error_messages','2017-05-04 03:19:14'),(12,'auth','0008_alter_user_username_max_length','2017-05-04 03:19:15'),(13,'personal','0001_initial','2017-05-04 03:19:15'),(14,'sessions','0001_initial','2017-05-04 03:19:15'),(15,'personal','0002_story_brief','2017-05-04 18:13:23');
/*!40000 ALTER TABLE `django_migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_session`
--

DROP TABLE IF EXISTS `django_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime NOT NULL,
  PRIMARY KEY (`session_key`),
  KEY `django_session_de54fa62` (`expire_date`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_session`
--

LOCK TABLES `django_session` WRITE;
/*!40000 ALTER TABLE `django_session` DISABLE KEYS */;
INSERT INTO `django_session` VALUES ('80ctjaeapxq1x84cvr09s3j5645eskwl','ZDBjNjcwNzM3ODhkYTVjZDIxNTA4YzUyZWI3ZTMzMjU5MWFmN2I0NDp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9oYXNoIjoiMGEyMTQxMTAzNDU4Mzg4YzNlZDU4NzY5OGM0ZjAyZmM5OThhOTZiYiIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=','2017-06-23 20:46:48'),('8bn3st3zv1zfewa8kj5qzdae5am0bm2t','ZGQ1OGI2OTRhYzEyMTMzNjI3YjkyNzNmNTQwMTYxNjBhOWFjM2E2MDp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiIyZTVjNjhkZGEwYmU3NWE0OTUxY2NjYmFjNzZjNTBiNDlhOTRmZGNhIn0=','2017-06-20 16:08:48'),('kgl9mvkj3bl2r0co0ehijgcitqzsiwh8','ZGQ1OGI2OTRhYzEyMTMzNjI3YjkyNzNmNTQwMTYxNjBhOWFjM2E2MDp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiIyZTVjNjhkZGEwYmU3NWE0OTUxY2NjYmFjNzZjNTBiNDlhOTRmZGNhIn0=','2017-05-18 03:40:28'),('n51sa4svnp449i1hjzjqhbo488l5y8ea','ZGQ1OGI2OTRhYzEyMTMzNjI3YjkyNzNmNTQwMTYxNjBhOWFjM2E2MDp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiIyZTVjNjhkZGEwYmU3NWE0OTUxY2NjYmFjNzZjNTBiNDlhOTRmZGNhIn0=','2017-05-20 11:08:37'),('yqbx2weh6i9l8xr61qztlhqpe16z3x0g','ZGQ1OGI2OTRhYzEyMTMzNjI3YjkyNzNmNTQwMTYxNjBhOWFjM2E2MDp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiIyZTVjNjhkZGEwYmU3NWE0OTUxY2NjYmFjNzZjNTBiNDlhOTRmZGNhIn0=','2017-05-20 10:42:50');
/*!40000 ALTER TABLE `django_session` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `personal_story`
--

DROP TABLE IF EXISTS `personal_story`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `personal_story` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `story_title` varchar(200) NOT NULL,
  `published` datetime NOT NULL,
  `writer` varchar(200) NOT NULL,
  `collection` varchar(200) NOT NULL,
  `cover_loc` varchar(400) NOT NULL,
  `likes` int(11) NOT NULL,
  `dislikes` int(11) NOT NULL,
  `read_count` int(11) NOT NULL,
  `storyfile` varchar(250) NOT NULL,
  `brief` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `personal_story`
--

LOCK TABLES `personal_story` WRITE;
/*!40000 ALTER TABLE `personal_story` DISABLE KEYS */;
INSERT INTO `personal_story` VALUES (1,'A Case Of Identity','2017-05-04 04:42:00','Sir Arthur Conan Doyle','The Adventures of Sherlock Holmes','https://c1.staticflickr.com/8/7027/6840251135_256de79611_b.jpg',0,0,55,'A_Case_of_Identity.txt','story of sherlock holmes adventures to thrill you'),(2,'A Scandal in Bohemia','2017-05-04 05:00:37','Arthur Conan Doyle','The Adventures of Sherlock Holmes','https://c1.staticflickr.com/8/7027/6840251135_256de79611_b.jpg',0,0,38,'A_SCANDAL_IN_BOHEMIA.txt','story of sherlock holmes adventures to thrill you'),(3,'Adventure of  The  Blue Carbuncle','2017-05-04 05:02:17','Sir Arthur Conan Doyle','The Adventures of Sherlock Holmes','https://c1.staticflickr.com/8/7027/6840251135_256de79611_b.jpg',0,0,18,'ADVENTURE_OF_THE_BLUE_CARBUNCLE.txt','story of sherlock holmes adventures to thrill you'),(4,'The Boscombe Valley Mystery','2017-05-04 05:05:43','Arthur Conan Doyle','The Adventures of Sherlock Holmes','https://c1.staticflickr.com/8/7027/6840251135_256de79611_b.jpg',0,0,12,'The_Boscombe_Valley_Mystery.txt','story of sherlock holmes adventures to thrill you'),(5,'Tthe Five Orange Pips','2017-05-06 11:30:08','Sir Arthur Conan Doyle','The Adventures of Sherlock Holmes','https://c1.staticflickr.com/8/7027/6840251135_256de79611_b.jpg',0,0,8,'https://c1.staticflickr.com/8/7027/6840251135_256de79611_b.jpg','Shelock Adventure'),(6,'THE MAN WITH THE TWISTED LIP','2017-05-06 11:35:08','Sir Arthur Conan Doyle','The Adventures of Sherlock Holmes','https://c1.staticflickr.com/8/7027/6840251135_256de79611_b.jpg',0,0,15,'THE_MAN_WITH_THE_TWISTED_LIP.txt','A Sherlock Advenure'),(7,'THE STORY OF ALADDIN','2017-05-06 11:42:55','Unanonymous','The Arabian Nights','https://upload.wikimedia.org/wikipedia/commons/a/a0/Aladdins_wonderful_lamp.jpeg',0,0,13,'aladdin.txt','Story of aladdin'),(8,'BEAUTY AND THE BEAST','2017-05-06 11:47:27','unanonymous','Unknown','http://www.publicdomainpictures.net/pictures/190000/velka/beauty-and-the-beast.jpg',0,0,15,'beautyandbeast.txt','Famous story of beauty and Beast'),(9,'ONE EYE, TWO EYES, THREE EYES','2017-05-06 11:51:34','unanonymous','Unknown','https://upload.wikimedia.org/wikipedia/commons/d/dd/Hermann_Vogel_One-Eye_Two-Eyes_and_Three-Eyes_2.jpg',0,0,12,'threesisters.txt','Story about three sisters'),(10,'BLUE BEARD','2017-05-06 11:54:00','unanonymous','Unknown','https://upload.wikimedia.org/wikipedia/commons/f/f1/Bluebeard.png',0,0,11,'bluebeard.txt','Rough stories'),(11,'THE BREMEN TOWN MUSICIANS:','2017-05-06 11:56:54','unanonymous','Unknown','bremenrownmusicians.jpg',0,0,21,'bremenrownmusicians.txt','Story of three musicians  of bewmen town'),(12,'CINDERELLA','2017-05-06 00:00:00','unanonymous','Unknown','cindrella.jpg',0,0,5,'cindrella.txt','Story of Cindrella'),(13,'THE FAIR ONE WITH GOLDEN LOCKS','2017-05-06 12:20:11','unanonymous','Unknown','https://www.umass.edu/AdelphiTheatreCalendar/image_18431230_424_hy_crop.jpg',0,0,12,'faironewithgoldenlocks.txt','Story about the golden locks'),(14,'THE GOLDEN GOOSE','2017-05-06 12:21:07','unanonymous','Unknown','https://upload.wikimedia.org/wikipedia/commons/3/3b/Simpleton_finds_The_Golden_Goose_-_Project_Gutenberg_eText_15661.jpg',0,0,5,'goldengoose.txt','Story of golden goose'),(15,'GRACIOSA AND PERCINET','2017-05-06 12:23:41','unanonymous','Unknown','https://topillustrations.files.wordpress.com/2014/01/graciosa-and-percinet.jpg',0,0,5,'graciosaandprecient.txt','Story of GRACIOSA AND PERCINET'),(16,'SNOW-WHITE AND ROSE-RED','2017-05-06 12:31:31','unanonymous','Unknown','https://topillustrations.files.wordpress.com/2014/01/snow-white-and-rose-red.jpg',0,0,8,'snowwhite.txt','Story of snow white princess'),(17,'THE MAGIC MIRROR','2017-05-06 15:14:53','unanonymous','Unknown','https://upload.wikimedia.org/wikipedia/commons/9/95/Magic_mirror.jpg',0,0,2,'magicmirror.txt','Story of magic mirror'),(18,'THE NOTHING EQUATION','2017-05-06 15:16:01','unanonymous','Unknown','nothingequation.png',0,0,4,'nothingequation.txt','Story of Nothin Equation'),(19,'A PAIR OF SILK STOCKINGS','2017-05-06 15:17:36','unanonymous','Unknown','http://1.bp.blogspot.com/_E7nSV2wzseg/SLCBmfKpNqI/AAAAAAAAAgQ/43TYbQuynr0/s320/story+part+2.png',0,0,5,'pairofsilkstockings.txt','Story of the silk stockings'),(20,'THE PRINCE WITH NOSE:','2017-05-06 15:19:07','unanonymous','Unknown','princewithnose.jpg',0,0,7,'princewithnose.txt','Story of prince with the nose'),(21,'PUSS IN BOOTS:','2017-05-06 15:20:42','unanonymous','Unknown','https://upload.wikimedia.org/wikipedia/commons/a/ab/Puss-in-boots-book.jpg',0,0,5,'pussinboots.txt','Story of Puss with the boots'),(22,'LITTLE RED-RIDING-HOOD:','2017-05-06 15:22:09','unanonymous','Unknown','https://upload.wikimedia.org/wikipedia/commons/7/7b/Little_Red_Riding_Hood_Meeting_the_Wolf.jpg',0,0,7,'redridinghood.txt','Story of Red Riging Hood'),(23,'THE SECOND VOYAGE OF SINDBAD THE SAILOR','2017-05-06 15:24:26','unanonymous','Unknown','https://upload.wikimedia.org/wikipedia/commons/0/0b/7th_voyage_of_Sinbad_-_Roc.png',0,0,8,'sinbad2 .txt','Story of adventures of Sindbad the sailor'),(24,'SLEEPING BEAUTY IN THE WOOD:','2017-05-06 15:26:02','unanonymous','Unknown','https://upload.wikimedia.org/wikipedia/commons/7/78/Prince_Florimund_finds_the_Sleeping_Beauty_-_Project_Gutenberg_etext_19993.jpg',0,0,8,'sleepingbeauty.txt','Story of Sleeping Beauty'),(25,'THE TELL-TALE HEART','2017-05-06 15:28:44','unanonymous','Unknown','https://upload.wikimedia.org/wikipedia/commons/thumb/6/6f/Harry_Clarke_The_Tell-Tale_Heart.jpg/580px-Harry_Clarke_The_Tell-Tale_Heart.jpg',0,0,2,'telltaleheart.txt','Famous tell tale heart story'),(26,'THE FROG-PRINCE','2017-05-06 15:30:12','unanonymous','Unknown','https://upload.wikimedia.org/wikipedia/commons/c/cb/Arthur_Rackham_Frog_Prince.jpg',0,0,2,'thefrogprince.txt','Story of the frog price'),(27,'The Gift of MAGI','2017-05-06 15:32:05','unanonymous','Unknown','https://upload.wikimedia.org/wikipedia/commons/thumb/5/55/William_Sydney_Porter.jpg/542px-William_Sydney_Porter.jpg',0,0,5,'thegiftofmagi.txt','Story about true love : \"The gift of MAGI\"'),(28,'THE THREE BEARS:','2017-05-06 15:34:47','unanonymous','Unknown','https://upload.wikimedia.org/wikipedia/commons/e/ef/The_Three_Bears_-_Project_Gutenberg_eText_17034.jpg',0,0,2,'threebears.txt','Story of three bears'),(29,'THE TWELVE BROTHERS','2017-05-06 15:37:00','unanonymous','Unknown','http://www.mainlesson.com/books/lang/red/zpage278.gif',0,0,6,'twelvebrothers','Story of the twelve  brothers'),(30,'THE UGLY DUCKLING','2017-05-06 15:38:11','unanonymous','Unknown','https://upload.wikimedia.org/wikipedia/commons/2/2a/The_Ugly_Duckling.jpg',0,0,3,'uglyduckling.txt','Story of an ugly duckling'),(31,'THE YELLOW DWARF:','2017-05-06 15:39:40','unanonymous','Unknown','https://upload.wikimedia.org/wikipedia/commons/e/e0/WalterCrane,yellowdwarf-cover.png',0,0,7,'yellowdwarf.txt','Story of the yellow dwarf'),(32,'The Time Machine','2017-06-10 11:30:08','H. G. (Herbert George) Wells','he Time Machine','https://c1.staticflickr.com/2/1052/5102696976_d815a1314b_b.jpg',0,0,3,'thetimemachine.txt','A short summary of H.G. Wells\'s The Time Machine.'),(33,'THE CRYSTAL EGG','2017-06-10 11:47:27','H. G. WELLS','Tales of Space and Time','https://upload.wikimedia.org/wikipedia/commons/5/54/H._G._Wells%2C_c.1890.jpg',0,0,2,'Thecrystalegg.txt','The story tells of a shop owner, named Mr. Cave, who finds a strange crystal egg that serves as a window into the planet Mars.'),(34,'Story of the Stone Age','2017-06-10 11:42:55','H. G. WELLS','Tales of Space and Time','https://upload.wikimedia.org/wikipedia/commons/5/54/H._G._Wells%2C_c.1890.jpg',0,0,6,'storyofstoneage.txt','Andoo, the huge cave bear, who lived in the cave up the gorge, had never even seen a man in all his wise and respectable life, until midway ..'),(35,'THe Star','2017-06-10 11:35:08','H. G. WELLS','Tales of Space and Time','https://upload.wikimedia.org/wikipedia/commons/5/54/H._G._Wells%2C_c.1890.jpg',0,0,2,'thefirsthorseman.txt','It was on the first day of the New Year that the announcement was made, almost simultaneously from three observatories, that the motion of the planet ...'),(36,'THE MAN WHO COULD WORK MIRACLES','2017-06-10 11:51:34','H. G. WELLS','Tales of Space and Time','https://upload.wikimedia.org/wikipedia/commons/5/54/H._G._Wells%2C_c.1890.jpg',0,0,0,'TheManWhoCouldWorkMiracles.txt','An ordinary man suddenly finds that anything he says comes true. Or at least, almost anything!'),(37,'THE COUNTRY OF THE BLIND','2017-05-06 11:54:00','H. G. WELLS','Unknown','https://upload.wikimedia.org/wikipedia/commons/5/54/H._G._Wells%2C_c.1890.jpg',0,0,3,'THE_COUNTRY_OF_THE_BLIND.txt','Three hundred miles and more from Chimborazo, one hundred from the snows of Cotopaxi, in the wildest wastes of Ecuador\'s Andes, there lies that mysterious mountain valley, cut off from all the world of men ...'),(38,'A DREAM OF ARMAGEDDON','2017-06-10 11:54:00','H. G. WELLS','British weekly magazine','https://upload.wikimedia.org/wikipedia/commons/5/54/H._G._Wells%2C_c.1890.jpg',0,0,0,'A_DREAM_OF_ARMAGEDDON.txt','The man with the white face entered the carriage at Rugby. He moved slowly in spite of the urgency of his porter, and even while he ...'),(39,'THE RED ROOM','1896-03-01 11:54:00','H. G. WELLS','The Idler magazine','https://upload.wikimedia.org/wikipedia/commons/5/54/H._G._Wells%2C_c.1890.jpg',0,0,2,'THE_RED_ROOM.txt','A short gothic story written by H. G. Wells in 1894. It was first published in the March 1896 edition of The Idler magazine.'),(40,'The Diamond Maker','1894-01-01 00:00:00','H. G. WELLS','Pall Mall Budget','https://upload.wikimedia.org/wikipedia/commons/5/54/H._G._Wells%2C_c.1890.jpg',0,0,1,'The_Diamond_Maker.txt','Some business had detained me in Chancery Lane nine in the evening, and thereafter, having some inkling of a headache, I was ...'),(41,'THE CONE','1895-09-18 00:00:00','H. G. WELLS','UNICORN','https://upload.wikimedia.org/wikipedia/commons/5/54/H._G._Wells%2C_c.1890.jpg',0,0,0,'THE_CONE.txt','The night was hot and overcast, the sky red, rimmed with the lingering sunset of mid-summer. They sat at the open window, trying to fancy the air was ..'),(42,'THE TRUTH ABOUT PYECRAFT','1903-01-01 00:00:00','H. G. WELLS','The Strand Magazine','https://upload.wikimedia.org/wikipedia/commons/5/54/H._G._Wells%2C_c.1890.jpg',0,0,1,'THE_TRUTH_ABOUT_PYECRAFT.txt','He sits not a dozen yards away. If I glance over my shoulder I can see him. And if I catch his eye--and usually I catch his eye-- it meets ...'),(43,'The Sea Raiders','1896-01-01 00:00:00','H. G. WELLS','short stories by Wells','https://upload.wikimedia.org/wikipedia/commons/5/54/H._G._Wells%2C_c.1890.jpg',0,0,0,'The_Sea_Raiders.txt','The Sea Raiders\" is a short story by H. G. Wells, first published in 1896 in The Weekly Sun Literary Supplement. It was included in The Plattner Story and Others, a collection of short stories by Wells published by Methuen & Co. in 1897.'),(44,'The Picture of Dorian Gray','1890-01-01 00:00:00','Oscar Wilde','BOOK','https://c1.staticflickr.com/6/5639/22433734869_dd7f90bcdc_b.jpg',0,0,5,'The_Picture_of_Dorian_Gray','The Picture of Dorian Gray is a philosophical novel by Oscar Wilde, first published complete in the July 1890 issue of Lippincott\'s Monthly Magazine'),(45,'THE JUNGLEBOOK.','1894-01-01 00:00:00','Rudyard Kipling','BOOK','https://upload.wikimedia.org/wikipedia/commons/5/57/The_Jungle_Book_%281910%29_cover.jpg',0,0,3,'THE_JUNGLEBOOK.txt','The Jungle Book is a collection of stories by English author Rudyard Kipling. The stories are fables, using animals in an anthropomorphic manner to give moral lessons.');
/*!40000 ALTER TABLE `personal_story` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-06-10  6:05:43
