module.exports = function(grunt) {

  // 1. All configuration goes here
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    // 1 - this is contecnate js files call them in one file only
    concat: {
      options: {
        separator: ';',
      },

       home: {
                src: ['personal/static/personal/js/jquery.min.js','personal/static/personal/js/bootstrap.min.js','personal/static/personal/js/jquery-ui.min.js'],
                dest: 'personal/static/personal/minjs/story_home.js',
             },
        storyread : {
            src: ['personal/static/personal/js/jquery.min.js','personal/static/personal/js/bootstrap.min.js','personal/static/personal/js/jquery-ui.min.js','personal/static/personal/js/query.easyPaginate.js',
                'personal/static/personal/js/jquery.twbsPagination.js'],
            dest: 'personal/static/personal/minjs/story_read.js',
         },

    },

    uglify: {
      build: {
        src: 'personal/static/personal/minjs/story_read.js',
        dest: 'personal/static/personal/minjs/story_read.min.js'
      }
    },

//     responsive_images: {
//     dev: {
//       files: [{
//         expand: true,
//         src: ['**/*.{jpg,gif,png}'],
//         cwd: 'personal/static/personal/Raw_Images',
//         dest: 'personal/static/personal/images/'
//       }]
//     }
//   },

  });

  // end 1 - this is contecnate js files call them in one file only

   // 3. Where we tell Grunt we plan to use this plug-in.
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  //grunt.loadNpmTasks('grunt-responsive-images');

  // 4. Where we tell Grunt what to do when we type "grunt" into the terminal.
  grunt.registerTask('default', ['concat', 'uglify']);

};
