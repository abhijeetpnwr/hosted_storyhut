from django.conf.urls import url,include
from django.contrib import admin
from django.contrib.sitemaps.views import sitemap
from personal.views import StorySitemap
from personal.views import StorySearchView
from personal.views import UserHomeView
from personal.views import StoryListView
from personal.views import StoryWriteView

from personal.views import StoryAutocomplete
from personal.views import StoryHutLoginview
from personal.views import StoryHutSignupview

from django.views.generic import TemplateView
from django.contrib.auth import views as auth_views

sitemaps = {'story' : StorySitemap}

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^',include('personal.urls')),
    url(r'^sitemap\.xml$', sitemap, {'sitemaps': sitemaps},name='django.contrib.sitemaps.views.sitemap'),
    url(r'^search/$', StorySearchView.as_view(),name="storylist"),
    url(r'^autocomplete/$',StoryAutocomplete.as_view(),name='utocomplete',),

   url(r'^/accounts/profile/', TemplateView.as_view(template_name="userboard.html")),
    # url(r'^accounts/profile/', TemplateView.as_view(template_name="personal/userboard.html")),
    url(r'^accounts/profile/', UserHomeView.as_view(),name="userdashboard"),
   url(r'^library/', StoryListView.as_view(),name="userdashboard"),
    url(r'^login/$', auth_views.login, name='login'),
    url(r'^logout/$', auth_views.logout,{'next_page': '/'}),
    url(r'^oauth/', include('social_django.urls', namespace='social')),  # <--
    url(r'^writestory/', StoryWriteView.as_view(),name='writestory',),
url(r'^signup_login/', StoryHutLoginview.as_view(),name='writestory',),
    url(r'^signup/', StoryHutSignupview.as_view(),name='writestory',),
    # You don't have to use "accounts," but I recommend it to keep
    # authentication separate from the rest of your app.
    url(r'^accounts/', include('allauth.urls')),


]

