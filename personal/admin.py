from django.contrib import admin
# Register your models here.

from .models import Story

class StoryModelAdmin(admin.ModelAdmin):
    list_display=["story_title","writer","collection","brief"]
    list_display_links=["story_title"]
    search_fields=["story_title"]
    class Meta:
        model = Story

exclude = ('slug',)

admin.site.register(Story,StoryModelAdmin)
