from django.db import models
from django.template.defaultfilters import slugify
from django.db.models.signals import pre_save
from django.contrib.auth.models import User

# Create your models here.


class Story(models.Model):
    story_title = models.CharField(max_length=200,null=True,blank=True) #Story title
    published = models.DateTimeField('date published') #Story published on storyhut
    writer = models.CharField(max_length=200) #WriterName
    collection=models.CharField(max_length=200) #Collection/Book name
    cover_loc=models.CharField(max_length=400) #Cover Art location
    brief= models.CharField(max_length=250)
    likes=models.IntegerField() #Totallikes
    dislikes=models.IntegerField() #Total dislikes
    read_count=models.IntegerField() #Story read time
    storyfile = models.CharField(max_length=250)
    Originally_published = models.CharField(max_length=250)
    Genre = models.CharField(max_length=100)
    Original_language = models.CharField(max_length=50)
    Adaptations = models.CharField(max_length=250)
    type = models.CharField(max_length=100)
    awards = models.CharField(max_length=250)
    slug = models.SlugField('story_title', max_length=100, unique=True,null=True,blank=True,)

    def upvote(self, user):
        try:
            self.post_votes.create(user=user, post=self, vote_type="up")
            self.likes += 1
            self.save()
        except IntegrityError:
            return 'already_upvoted'
        return 'ok'


    def downvote(self, user):
        try:
            self.post_votes.create(user=user, post=self, vote_type="down")
            self.dislikes += 1
            self.save()
        except IntegrityError:
            return 'already_downvoted'
        return 'ok'


    def display_text_file(self):
        print("Ha ha haa")
        return "sample text"

    def get_absolute_url(self):
        return "/personal/"+self.slug

class UserVotes(models.Model):
    user = models.ForeignKey(User, related_name="user_votes")
    post = models.ForeignKey(Story, related_name="post_votes")
    vote_type = models.CharField(max_length=200)


    class Meta:
        unique_together = ('user', 'post', 'vote_type')

def pre_save_post_receiver(sender,instance,*args,**kwargs):
    slug = slugify(instance.story_title)
    exists = Story.objects.filter(slug=slug).exists
    if exists:
        slug= "%s-%s" %(slug,instance.id)
    instance.slug = slug

pre_save.connect(pre_save_post_receiver,sender=Story)


