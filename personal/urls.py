
from django.conf.urls import url,include
#from django.contrib import admin
from personal.models import Story
from django.views.generic import ListView, DetailView
from . import  views

handler404 = 'personal.views.handler404'
handler500 = 'personal.views.handler404'
handler403 = 'mysite.views.my_custom_permission_denied_view'
handler400 = 'mysite.views.my_custom_bad_request_view'

urlpatterns = [


    #url(r'^$', views.StoryListView.as_view(queryset=Story.objects.all(),template_name="personal/home.html")),

    url(r'^$', views.StoryListView.as_view(queryset=Story.objects.all(),template_name="personal/home.html"),name="storylist"),

    url(r'^google87af252086623517.html$', ListView.as_view(
                                    queryset=Story.objects.all(),
                                    template_name="personal/google87af252086623517.html")),

    url(r'personal\/(?P<slug>.+)\/$',views.detailView.as_view(), name="detail"),


]

