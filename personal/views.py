from django.shortcuts import render
from django.views import generic
from .models import  Story
from django.templatetags.static import static
import os
import re
from django.contrib.sitemaps import Sitemap
from django.db.models import Q
import json

from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.contrib.auth import login, authenticate
from .forms import SignupForm
from django.contrib.sites.shortcuts import get_current_site
from django.utils.encoding import force_bytes, force_text
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.template.loader import render_to_string
from .tokens import account_activation_token
from django.contrib.auth.models import User
from django.core.mail import EmailMessage


def index(request):
    mylist=[15,13,20,25]
    context = {'mylist':mylist}
    return render(request, 'personal/home.html',context)

class StorySitemap(Sitemap):
    changefreq = "daily"
    priority = 1.0

    def items(self):
        return Story.objects.all()

    def lastmod(self, obj):
        return obj.published


class StorySearchView(generic.ListView):
    model = Story
    template_name = "personal/home.html"

    def get_context_data(self, **kwargs):
        context = super(StorySearchView, self).get_context_data(**kwargs)
        context['now'] = [15,13,18]
        return context

    def get_queryset(self):
        qs = Story.objects.all()
        toget = self.request.GET.get("q")
        print(" to find :",toget)
        #print("returned :",qs)
        qs = qs.filter(Q(story_title__icontains=toget) | Q(brief__icontains=toget) | Q(writer__icontains=toget) |  Q(collection__icontains=toget))

        print("test:--",qs)
        return  qs

class UserHomeView(generic.ListView):

    template_name="personal/home.html"

    def get_context_data(self, **kwargs):
        mylist = list(self.request.user.social_auth.values_list('uid')[0])
        print(mylist)
        context = {
            'uid': mylist[0],
            "now":[15,13,16],
            "queryset" : Story.objects.all().order_by('read_count'),
            }
        self.request.session['uid'] = mylist[0]
        return context

    queryset = Story.objects.all().order_by('read_count'),


class StoryListView(generic.ListView):
    model = Story

    def get_context_data(self, **kwargs):
        context = super(StoryListView, self).get_context_data(**kwargs)
        context['now'] = [15,13,18]
        return context

    template_name="personal/bookshelf.html"

    context ={
        "queryset" : Story.objects.all().order_by('read_count'),
        "now":[15,13,16]
    }




class StoryHomeView(generic.ListView):
    model = Story

    def get_context_data(self, **kwargs):
        context = super(StoryListView, self).get_context_data(**kwargs)
        context['now'] = [15,13,18]
        return context

    template_name="personal/home.html"

    context ={
        "queryset" : Story.objects.all().order_by('read_count'),
        "now":[15,13,16]
    }

class StoryHutLoginview(generic.ListView):

    model = Story

    template_name="personal/login-signup.html"

    def get_context_data(self, **kwargs):
        context = {}
        return context

class StoryHutSignupview(generic.ListView):
    model = Story

    template_name="personal/signup.html"

    def get_context_data(self, **kwargs):
        context = {}
        return context




class StoryWriteView(generic.ListView):

    model = Story

    template_name="personal/writestory.html"

    def get_context_data(self, **kwargs):
        context = {}
        return context


def signup(request):
    if request.method == 'POST':
        form = SignupForm(request.POST)
        if form.is_valid():
            user = form.save(commit=False)
            user.is_active = False
            user.save()
            current_site = get_current_site(request)
            message = render_to_string('acc_active_email.html', {
                'user': user,
                'domain': current_site.domain,
                'uid': urlsafe_base64_encode(force_bytes(user.pk)),
                'token': account_activation_token.make_token(user),
            })
            mail_subject = 'Activate your blog account.'
            to_email = form.cleaned_data.get('email')
            email = EmailMessage(mail_subject, message, to=[to_email])
            email.send()
            return HttpResponse('Please confirm your email address to complete the registration')

    else:
        form = SignupForm()

    return render(request, 'signup.html', {'form': form})


def activate(request, uidb64, token):
    try:
        uid = force_text(urlsafe_base64_decode(uidb64))
        user = User.objects.get(pk=uid)
    except(TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None
    if user is not None and account_activation_token.check_token(user, token):
        user.is_active = True
        user.save()
        login(request, user)
        # return redirect('home')
        return HttpResponse('Thank you for your email confirmation. Now you can login your account.')
    else:
        return HttpResponse('Activation link is invalid!')



class StoryAutocomplete(generic.ListView):
    def get(self, request, *args, **kwargs):
        data = request.GET

        print(" Reached autcomplete")
        toget = data.get("term")

        qs = Story.objects.all()

        qs = qs.filter(Q(story_title__icontains=toget) | Q(brief__icontains=toget) | Q(writer__icontains=toget) |  Q(collection__icontains=toget))

        results = []

        for story in qs:
           writer = story.writer
           if "anony" in story.writer:
               writer = ""


           user_json = {}

           user_json['id'] = story.id

           user_json['label'] = writer+" "+story.story_title

           user_json['value'] = story.story_title
           results.append(user_json)

        data = json.dumps(results)

        mimetype = 'application/json'

        return HttpResponse(data, mimetype)


class detailView(generic.DetailView):
    model = Story
    template_name = 'personal/storydetail.html'
    slug_url_kwarg = 'slug'
    query_pk_and_slug = True

    def checkIfRomanNumeral(self,numeral):
         """Controls that the userinput only contains valid roman numerals"""
         numeral = numeral.upper()
         validRomanNumerals = ["M", "D", "C", "L", "X", "V", "I", "(", ")"]
         for letters in numeral:
            if letters not in validRomanNumerals:
                print("Sorry that is not a valid roman numeral")
                return False
         return True

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        context = self.get_context_data(object=self.object)
        story_file = context.get("story").storyfile
        story_obj  = context.get("story")
        story_obj.read_count = story_obj.read_count+1
        story_obj.save()
        lines = ""
        tag_counter = 0
        content = "<p class='firstpara'>"
        isFirstChapter = True

        lastLinePageBreak = False

        isInPre = False
        with open(mysite.settings.BASE_DIR+'/Stories/'+story_file, 'r') as content_file:
            #content = content_file.read()

            for line in content_file:

                if "<pre" in line:
                    isInPre = True

                if "</pre>" in line:
                    isInPre = False

                if re.match(r'^\s*$', line) and not isInPre:
                    line="</p><p>"
                    tag_counter = tag_counter+1


                if line.strip().lower().startswith("chapter") or ( len(line)<20 and self.checkIfRomanNumeral(line)):
                    if lastLinePageBreak or isFirstChapter:
                        line = "<h2 class='chapter-header'>"+line+"</h2>"
                        isFirstChapter = False
                    else:
                        line = "$PB$"+"<h2 class='chapter-header'>"+line+"</h2>"

                content = content+line

                if tag_counter == 20:
                    tag_counter = 0
                    content = content+"</p> $PB$"+"<p>"

                if "$PB$" in line:
                    lastLinePageBreak = True
                    tag_counter = 0

        content = content+"<hr style='border-top: 1px dashed #37424A;border-bottom: none;margin-bottom: 20px;max-width: 80%;'>"

        context['story_text'] = content
        context['story'] = story_obj
        return self.render_to_response(context)



