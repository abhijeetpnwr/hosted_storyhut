-- MySQL dump 10.13  Distrib 5.5.54, for debian-linux-gnu (x86_64)
--
-- Host: abhijeetpnwr.mysql.pythonanywhere-services.com    Database: abhijeetpnwr$storyhut
-- ------------------------------------------------------
-- Server version	5.6.27-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `auth_group`
--

DROP TABLE IF EXISTS `auth_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group`
--

LOCK TABLES `auth_group` WRITE;
/*!40000 ALTER TABLE `auth_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_group_permissions`
--

DROP TABLE IF EXISTS `auth_group_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_group_permissions_group_id_0cd325b0_uniq` (`group_id`,`permission_id`),
  KEY `auth_group_permissi_permission_id_84c5c92e_fk_auth_permission_id` (`permission_id`),
  CONSTRAINT `auth_group_permissi_permission_id_84c5c92e_fk_auth_permission_id` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `auth_group_permissions_group_id_b120cbf9_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group_permissions`
--

LOCK TABLES `auth_group_permissions` WRITE;
/*!40000 ALTER TABLE `auth_group_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_permission`
--

DROP TABLE IF EXISTS `auth_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_permission_content_type_id_01ab375a_uniq` (`content_type_id`,`codename`),
  CONSTRAINT `auth_permissi_content_type_id_2f476e4b_fk_django_content_type_id` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_permission`
--

LOCK TABLES `auth_permission` WRITE;
/*!40000 ALTER TABLE `auth_permission` DISABLE KEYS */;
INSERT INTO `auth_permission` VALUES (1,'Can add story',1,'add_story'),(2,'Can change story',1,'change_story'),(3,'Can delete story',1,'delete_story'),(4,'Can add log entry',2,'add_logentry'),(5,'Can change log entry',2,'change_logentry'),(6,'Can delete log entry',2,'delete_logentry'),(7,'Can add user',3,'add_user'),(8,'Can change user',3,'change_user'),(9,'Can delete user',3,'delete_user'),(10,'Can add permission',4,'add_permission'),(11,'Can change permission',4,'change_permission'),(12,'Can delete permission',4,'delete_permission'),(13,'Can add group',5,'add_group'),(14,'Can change group',5,'change_group'),(15,'Can delete group',5,'delete_group'),(16,'Can add content type',6,'add_contenttype'),(17,'Can change content type',6,'change_contenttype'),(18,'Can delete content type',6,'delete_contenttype'),(19,'Can add session',7,'add_session'),(20,'Can change session',7,'change_session'),(21,'Can delete session',7,'delete_session'),(22,'Can add nonce',8,'add_nonce'),(23,'Can change nonce',8,'change_nonce'),(24,'Can delete nonce',8,'delete_nonce'),(25,'Can add code',9,'add_code'),(26,'Can change code',9,'change_code'),(27,'Can delete code',9,'delete_code'),(28,'Can add user social auth',10,'add_usersocialauth'),(29,'Can change user social auth',10,'change_usersocialauth'),(30,'Can delete user social auth',10,'delete_usersocialauth'),(31,'Can add association',11,'add_association'),(32,'Can change association',11,'change_association'),(33,'Can delete association',11,'delete_association'),(34,'Can add partial',12,'add_partial'),(35,'Can change partial',12,'change_partial'),(36,'Can delete partial',12,'delete_partial'),(37,'Can add user votes',13,'add_uservotes'),(38,'Can change user votes',13,'change_uservotes'),(39,'Can delete user votes',13,'delete_uservotes');
/*!40000 ALTER TABLE `auth_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user`
--

DROP TABLE IF EXISTS `auth_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(128) NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(150) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(30) NOT NULL,
  `email` varchar(254) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user`
--

LOCK TABLES `auth_user` WRITE;
/*!40000 ALTER TABLE `auth_user` DISABLE KEYS */;
INSERT INTO `auth_user` VALUES (1,'pbkdf2_sha256$30000$S1Hegsm1tUSs$q2vZeojTfmdPRexBAgPoBWE0u+dWox8IAKyN9P8D6sw=','2017-09-02 11:22:59',1,'abhijeet','Abhijeet','Panwar','abhijeetpnwr@gmail.com',1,1,'2017-05-04 03:39:42'),(2,'!ZgaRIkKRJBeCwOrLrXR4TUY1WdgS2sl6RnEZnYMy','2017-06-25 08:33:28',0,'StorytestSingh','Storytest','Singh','',0,1,'2017-06-25 08:33:28'),(3,'!dbSqclKWffaiT29gDMT656QkfJMeXAV605D1pGeO','2017-08-28 11:25:22',0,'RajinderGupta','Rajinder','Gupta','',0,1,'2017-06-26 07:53:05'),(4,'!Mj95ciXPcExoTDk7gE9vZUodc0uy0HAsGvvCG5Vd','2017-06-26 08:57:04',0,'GazalDeepKaur','Gazal','Deep Kaur','',0,1,'2017-06-26 08:57:04'),(5,'!DXwrOmbIfgp9ZCoeuut2mOLkFM8GA48b338WbxkD','2017-08-27 07:27:48',0,'ShwetaSharmaGupta','Shweta','Sharma Gupta','',0,1,'2017-08-27 07:27:48');
/*!40000 ALTER TABLE `auth_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_groups`
--

DROP TABLE IF EXISTS `auth_user_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_user_groups_user_id_94350c0c_uniq` (`user_id`,`group_id`),
  KEY `auth_user_groups_group_id_97559544_fk_auth_group_id` (`group_id`),
  CONSTRAINT `auth_user_groups_group_id_97559544_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  CONSTRAINT `auth_user_groups_user_id_6a12ed8b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_groups`
--

LOCK TABLES `auth_user_groups` WRITE;
/*!40000 ALTER TABLE `auth_user_groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_user_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_user_permissions`
--

DROP TABLE IF EXISTS `auth_user_user_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user_user_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_user_user_permissions_user_id_14a6b632_uniq` (`user_id`,`permission_id`),
  KEY `auth_user_user_perm_permission_id_1fbb5f2c_fk_auth_permission_id` (`permission_id`),
  CONSTRAINT `auth_user_user_perm_permission_id_1fbb5f2c_fk_auth_permission_id` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_user_permissions`
--

LOCK TABLES `auth_user_user_permissions` WRITE;
/*!40000 ALTER TABLE `auth_user_user_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_user_user_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_admin_log`
--

DROP TABLE IF EXISTS `django_admin_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action_time` datetime NOT NULL,
  `object_id` longtext,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) unsigned NOT NULL,
  `change_message` longtext NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `django_admin__content_type_id_c4bce8eb_fk_django_content_type_id` (`content_type_id`),
  KEY `django_admin_log_user_id_c564eba6_fk_auth_user_id` (`user_id`),
  CONSTRAINT `django_admin__content_type_id_c4bce8eb_fk_django_content_type_id` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`),
  CONSTRAINT `django_admin_log_user_id_c564eba6_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=304 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_admin_log`
--

LOCK TABLES `django_admin_log` WRITE;
/*!40000 ALTER TABLE `django_admin_log` DISABLE KEYS */;
INSERT INTO `django_admin_log` VALUES (1,'2017-05-04 04:51:06','1','Story object',1,'[{\"added\": {}}]',1,1),(2,'2017-05-04 05:00:41','2','Story object',1,'[{\"added\": {}}]',1,1),(3,'2017-05-04 05:04:39','3','Story object',1,'[{\"added\": {}}]',1,1),(4,'2017-05-04 05:07:00','4','Story object',1,'[{\"added\": {}}]',1,1),(5,'2017-05-04 17:05:34','4','Story object',2,'[{\"changed\": {\"fields\": [\"cover_loc\"]}}]',1,1),(6,'2017-05-04 17:06:50','3','Story object',2,'[{\"changed\": {\"fields\": [\"cover_loc\"]}}]',1,1),(7,'2017-05-04 17:06:59','2','Story object',2,'[{\"changed\": {\"fields\": [\"cover_loc\"]}}]',1,1),(8,'2017-05-04 17:07:06','1','Story object',2,'[{\"changed\": {\"fields\": [\"cover_loc\"]}}]',1,1),(9,'2017-05-06 10:43:45','3','Story object',2,'[{\"changed\": {\"fields\": [\"story_title\", \"brief\"]}}]',1,1),(10,'2017-05-06 11:08:50','4','Story object',2,'[{\"changed\": {\"fields\": [\"cover_loc\", \"brief\"]}}]',1,1),(11,'2017-05-06 11:09:00','3','Story object',2,'[{\"changed\": {\"fields\": [\"cover_loc\"]}}]',1,1),(12,'2017-05-06 11:09:08','2','Story object',2,'[{\"changed\": {\"fields\": [\"cover_loc\", \"brief\"]}}]',1,1),(13,'2017-05-06 11:09:17','1','Story object',2,'[{\"changed\": {\"fields\": [\"cover_loc\", \"brief\"]}}]',1,1),(14,'2017-05-06 11:12:34','4','Story object',2,'[]',1,1),(15,'2017-05-06 11:12:44','3','Story object',2,'[]',1,1),(16,'2017-05-06 11:12:54','2','Story object',2,'[]',1,1),(17,'2017-05-06 11:13:05','1','Story object',2,'[]',1,1),(18,'2017-05-06 11:15:47','4','Story object',2,'[{\"changed\": {\"fields\": [\"cover_loc\"]}}]',1,1),(19,'2017-05-06 11:16:08','3','Story object',2,'[{\"changed\": {\"fields\": [\"cover_loc\"]}}]',1,1),(20,'2017-05-06 11:16:19','2','Story object',2,'[{\"changed\": {\"fields\": [\"cover_loc\"]}}]',1,1),(21,'2017-05-06 11:16:29','1','Story object',2,'[{\"changed\": {\"fields\": [\"cover_loc\"]}}]',1,1),(22,'2017-05-06 11:32:25','5','Story object',1,'[{\"added\": {}}]',1,1),(23,'2017-05-06 11:35:55','6','Story object',1,'[{\"added\": {}}]',1,1),(24,'2017-05-06 11:45:32','7','Story object',1,'[{\"added\": {}}]',1,1),(25,'2017-05-06 11:46:22','7','Story object',2,'[{\"changed\": {\"fields\": [\"writer\"]}}]',1,1),(26,'2017-05-06 11:48:59','8','Story object',1,'[{\"added\": {}}]',1,1),(27,'2017-05-06 11:52:28','9','Story object',1,'[{\"added\": {}}]',1,1),(28,'2017-05-06 11:54:49','10','Story object',1,'[{\"added\": {}}]',1,1),(29,'2017-05-06 11:58:06','11','Story object',1,'[{\"added\": {}}]',1,1),(30,'2017-05-06 12:17:46','12','Story object',1,'[{\"added\": {}}]',1,1),(31,'2017-05-06 12:20:13','13','Story object',1,'[{\"added\": {}}]',1,1),(32,'2017-05-06 12:22:13','14','Story object',1,'[{\"added\": {}}]',1,1),(33,'2017-05-06 12:24:29','15','Story object',1,'[{\"added\": {}}]',1,1),(34,'2017-05-06 12:32:16','16','Story object',1,'[{\"added\": {}}]',1,1),(35,'2017-05-06 15:15:35','17','Story object',1,'[{\"added\": {}}]',1,1),(36,'2017-05-06 15:16:47','18','Story object',1,'[{\"added\": {}}]',1,1),(37,'2017-05-06 15:18:18','19','Story object',1,'[{\"added\": {}}]',1,1),(38,'2017-05-06 15:19:42','20','Story object',1,'[{\"added\": {}}]',1,1),(39,'2017-05-06 15:21:24','21','Story object',1,'[{\"added\": {}}]',1,1),(40,'2017-05-06 15:23:38','22','Story object',1,'[{\"added\": {}}]',1,1),(41,'2017-05-06 15:25:17','23','Story object',1,'[{\"added\": {}}]',1,1),(42,'2017-05-06 15:26:51','24','Story object',1,'[{\"added\": {}}]',1,1),(43,'2017-05-06 15:29:03','25','Story object',1,'[{\"added\": {}}]',1,1),(44,'2017-05-06 15:30:39','26','Story object',1,'[{\"added\": {}}]',1,1),(45,'2017-05-06 15:33:03','27','Story object',1,'[{\"added\": {}}]',1,1),(46,'2017-05-06 15:34:55','28','Story object',1,'[{\"added\": {}}]',1,1),(47,'2017-05-06 15:37:27','29','Story object',1,'[{\"added\": {}}]',1,1),(48,'2017-05-06 15:38:53','30','Story object',1,'[{\"added\": {}}]',1,1),(49,'2017-05-06 15:40:12','31','Story object',1,'[{\"added\": {}}]',1,1),(50,'2017-06-09 20:54:30','32','Story object',1,'[{\"added\": {}}]',1,1),(51,'2017-06-09 21:01:18','32','Story object',2,'[{\"changed\": {\"fields\": [\"cover_loc\"]}}]',1,1),(52,'2017-06-09 21:13:32','33','Story object',1,'[{\"added\": {}}]',1,1),(53,'2017-06-09 21:20:19','34','Story object',1,'[{\"added\": {}}]',1,1),(54,'2017-06-10 03:26:54','34','Story object',2,'[{\"changed\": {\"fields\": [\"cover_loc\"]}}]',1,1),(55,'2017-06-10 03:38:59','35','Story object',1,'[{\"added\": {}}]',1,1),(56,'2017-06-10 03:57:53','34','Story object',2,'[{\"changed\": {\"fields\": [\"story_title\"]}}]',1,1),(57,'2017-06-10 04:02:39','34','Story object',2,'[{\"changed\": {\"fields\": [\"storyfile\"]}}]',1,1),(58,'2017-06-10 04:06:21','35','Story object',2,'[{\"changed\": {\"fields\": [\"story_title\", \"brief\"]}}]',1,1),(59,'2017-06-10 04:07:36','35','Story object',2,'[{\"changed\": {\"fields\": [\"brief\"]}}]',1,1),(60,'2017-06-10 04:13:46','36','Story object',1,'[{\"added\": {}}]',1,1),(61,'2017-06-10 04:15:21','33','Story object',2,'[{\"changed\": {\"fields\": [\"cover_loc\"]}}]',1,1),(62,'2017-06-10 04:23:34','37','Story object',1,'[{\"added\": {}}]',1,1),(63,'2017-06-10 04:31:08','38','Story object',1,'[{\"added\": {}}]',1,1),(64,'2017-06-10 04:33:56','37','Story object',2,'[{\"changed\": {\"fields\": [\"brief\"]}}]',1,1),(65,'2017-06-10 04:34:44','37','Story object',2,'[{\"changed\": {\"fields\": [\"brief\"]}}]',1,1),(66,'2017-06-10 04:44:52','39','Story object',1,'[{\"added\": {}}]',1,1),(67,'2017-06-10 04:46:47','39','Story object',2,'[{\"changed\": {\"fields\": [\"brief\"]}}]',1,1),(68,'2017-06-10 04:52:29','40','Story object',1,'[{\"added\": {}}]',1,1),(69,'2017-06-10 04:59:41','41','Story object',1,'[{\"added\": {}}]',1,1),(70,'2017-06-10 05:04:56','42','Story object',1,'[{\"added\": {}}]',1,1),(71,'2017-06-10 05:10:56','43','Story object',1,'[{\"added\": {}}]',1,1),(72,'2017-06-10 05:34:12','44','Story object',1,'[{\"added\": {}}]',1,1),(73,'2017-06-10 05:37:22','44','Story object',2,'[{\"changed\": {\"fields\": [\"cover_loc\"]}}]',1,1),(74,'2017-06-10 05:48:51','45','Story object',1,'[{\"added\": {}}]',1,1),(75,'2017-06-10 06:27:49','45','Story object',2,'[{\"changed\": {\"fields\": [\"Genre\", \"Adaptations\", \"type\"]}}]',1,1),(76,'2017-06-10 06:28:56','45','Story object',2,'[{\"changed\": {\"fields\": [\"published\", \"Originally_published\"]}}]',1,1),(77,'2017-06-10 07:09:06','45','Story object',2,'[]',1,1),(78,'2017-06-10 07:33:46','24','Story object',2,'[{\"changed\": {\"fields\": [\"Genre\", \"Adaptations\"]}}]',1,1),(79,'2017-06-10 07:35:45','44','Story object',2,'[{\"changed\": {\"fields\": [\"Originally_published\", \"Genre\", \"Adaptations\"]}}]',1,1),(80,'2017-06-10 07:36:47','44','Story object',2,'[{\"changed\": {\"fields\": [\"type\"]}}]',1,1),(81,'2017-06-10 07:41:44','1','Story object',2,'[{\"changed\": {\"fields\": [\"brief\", \"Originally_published\", \"Genre\", \"Adaptations\"]}}]',1,1),(82,'2017-06-10 07:43:09','1','Story object',2,'[{\"changed\": {\"fields\": [\"brief\"]}}]',1,1),(83,'2017-06-10 08:09:15','46','Story object',1,'[{\"added\": {}}]',1,1),(84,'2017-06-11 08:41:38','2','Story object',2,'[{\"changed\": {\"fields\": [\"brief\", \"Originally_published\", \"Genre\", \"Adaptations\"]}}]',1,1),(85,'2017-06-11 08:42:50','2','Story object',2,'[{\"changed\": {\"fields\": [\"brief\"]}}]',1,1),(86,'2017-06-11 08:44:25','3','Story object',2,'[{\"changed\": {\"fields\": [\"brief\", \"Originally_published\", \"Genre\", \"Adaptations\"]}}]',1,1),(87,'2017-06-11 08:47:01','4','Story object',2,'[{\"changed\": {\"fields\": [\"brief\", \"Genre\", \"Adaptations\"]}}]',1,1),(88,'2017-06-11 08:49:44','5','Story object',2,'[{\"changed\": {\"fields\": [\"brief\", \"Originally_published\", \"Genre\", \"Adaptations\"]}}]',1,1),(89,'2017-06-11 08:52:18','6','Story object',2,'[{\"changed\": {\"fields\": [\"brief\", \"Originally_published\", \"Genre\", \"Adaptations\"]}}]',1,1),(90,'2017-06-11 09:01:00','7','Story object',2,'[{\"changed\": {\"fields\": [\"writer\", \"brief\", \"Originally_published\", \"Genre\", \"Adaptations\"]}}]',1,1),(91,'2017-06-11 09:05:21','8','Story object',2,'[{\"changed\": {\"fields\": [\"brief\", \"Genre\", \"Adaptations\"]}}]',1,1),(92,'2017-06-11 09:15:23','9','Story object',2,'[{\"changed\": {\"fields\": [\"writer\", \"collection\", \"brief\", \"Genre\", \"Original_language\", \"Adaptations\"]}}]',1,1),(93,'2017-06-11 09:16:15','9','Story object',2,'[{\"changed\": {\"fields\": [\"brief\"]}}]',1,1),(94,'2017-06-11 09:20:32','10','Story object',2,'[{\"changed\": {\"fields\": [\"writer\", \"brief\", \"Originally_published\", \"Genre\", \"Adaptations\"]}}]',1,1),(95,'2017-06-11 09:23:52','11','Story object',2,'[{\"changed\": {\"fields\": [\"writer\", \"brief\", \"Originally_published\", \"Genre\", \"Adaptations\"]}}]',1,1),(96,'2017-06-11 09:32:42','11','Story object',2,'[{\"changed\": {\"fields\": [\"brief\", \"Original_language\"]}}]',1,1),(97,'2017-06-11 09:38:35','12','Story object',2,'[{\"changed\": {\"fields\": [\"writer\", \"collection\", \"brief\", \"Originally_published\", \"Genre\", \"Adaptations\"]}}]',1,1),(98,'2017-06-11 09:39:35','12','Story object',2,'[{\"changed\": {\"fields\": [\"brief\"]}}]',1,1),(99,'2017-06-11 09:46:49','15','Story object',2,'[{\"changed\": {\"fields\": [\"writer\", \"collection\", \"brief\", \"Originally_published\", \"Genre\", \"Original_language\", \"Adaptations\"]}}]',1,1),(100,'2017-06-11 09:51:11','14','Story object',2,'[{\"changed\": {\"fields\": [\"writer\", \"brief\", \"Originally_published\", \"Genre\", \"Adaptations\"]}}]',1,1),(101,'2017-06-11 09:55:16','13','Story object',2,'[{\"changed\": {\"fields\": [\"writer\", \"collection\", \"brief\", \"Genre\", \"Adaptations\"]}}]',1,1),(102,'2017-06-11 09:59:52','26','Story object',2,'[{\"changed\": {\"fields\": [\"writer\", \"brief\", \"Originally_published\", \"Genre\", \"Original_language\", \"Adaptations\"]}}]',1,1),(103,'2017-06-11 10:02:02','25','Story object',2,'[{\"changed\": {\"fields\": [\"writer\", \"brief\", \"Originally_published\", \"Genre\", \"Adaptations\"]}}]',1,1),(104,'2017-06-11 10:08:27','24','Story object',2,'[{\"changed\": {\"fields\": [\"writer\", \"collection\", \"read_count\", \"Originally_published\", \"Genre\"]}}]',1,1),(105,'2017-06-11 10:12:01','23','Story object',2,'[{\"changed\": {\"fields\": [\"collection\", \"brief\", \"Originally_published\", \"Genre\", \"Adaptations\"]}}]',1,1),(106,'2017-06-11 10:15:07','22','Story object',2,'[{\"changed\": {\"fields\": [\"story_title\", \"writer\", \"collection\", \"brief\", \"Originally_published\", \"Genre\", \"Original_language\", \"Adaptations\"]}}]',1,1),(107,'2017-06-11 10:18:04','21','Story object',2,'[{\"changed\": {\"fields\": [\"story_title\", \"writer\", \"collection\", \"brief\", \"Originally_published\", \"Genre\", \"Adaptations\"]}}]',1,1),(108,'2017-06-11 16:17:04','16','Story object',2,'[{\"changed\": {\"fields\": [\"writer\", \"collection\", \"brief\", \"Originally_published\", \"Genre\", \"Original_language\", \"Adaptations\"]}}]',1,1),(109,'2017-06-11 16:21:15','32','Story object',2,'[{\"changed\": {\"fields\": [\"brief\", \"Originally_published\", \"Genre\", \"Adaptations\"]}}]',1,1),(110,'2017-06-11 16:25:20','31','Story object',2,'[{\"changed\": {\"fields\": [\"story_title\", \"writer\", \"collection\", \"brief\", \"Genre\", \"Original_language\", \"Adaptations\"]}}]',1,1),(111,'2017-06-11 16:29:20','30','Story object',2,'[{\"changed\": {\"fields\": [\"writer\", \"brief\", \"Originally_published\", \"Genre\", \"Original_language\", \"Adaptations\"]}}]',1,1),(112,'2017-06-11 17:15:58','29','Story object',2,'[{\"changed\": {\"fields\": [\"writer\", \"collection\", \"brief\", \"Genre\", \"Adaptations\"]}}]',1,1),(113,'2017-06-11 17:21:20','28','Story object',2,'[{\"changed\": {\"fields\": [\"story_title\", \"writer\", \"brief\", \"read_count\", \"Originally_published\", \"Genre\", \"Adaptations\"]}}]',1,1),(114,'2017-06-11 17:25:50','27','Story object',2,'[{\"changed\": {\"fields\": [\"writer\", \"collection\", \"brief\", \"Genre\", \"Adaptations\"]}}]',1,1),(115,'2017-06-11 17:43:03','20','Story object',2,'[{\"changed\": {\"fields\": [\"story_title\", \"writer\", \"collection\", \"brief\", \"Originally_published\", \"Genre\", \"Adaptations\"]}}]',1,1),(116,'2017-06-11 17:45:06','19','Story object',2,'[{\"changed\": {\"fields\": [\"writer\", \"brief\", \"Originally_published\", \"Genre\", \"Adaptations\"]}}]',1,1),(117,'2017-06-11 17:47:46','18','Story object',2,'[{\"changed\": {\"fields\": [\"writer\", \"brief\", \"read_count\", \"Genre\", \"Adaptations\"]}}]',1,1),(118,'2017-06-11 18:24:27','47','Story object',1,'[{\"added\": {}}]',1,1),(119,'2017-06-11 18:25:44','47','Story object',2,'[{\"changed\": {\"fields\": [\"story_title\"]}}]',1,1),(120,'2017-06-11 18:26:25','47','Story object',2,'[{\"changed\": {\"fields\": [\"brief\"]}}]',1,1),(121,'2017-06-11 18:41:55','48','Story object',1,'[{\"added\": {}}]',1,1),(122,'2017-06-11 18:59:06','49','Story object',1,'[{\"added\": {}}]',1,1),(123,'2017-06-11 19:00:28','49','Story object',2,'[{\"changed\": {\"fields\": [\"brief\"]}}]',1,1),(124,'2017-06-11 20:06:44','50','Story object',1,'[{\"added\": {}}]',1,1),(125,'2017-06-11 20:30:44','51','Story object',1,'[{\"added\": {}}]',1,1),(126,'2017-06-11 20:30:44','52','Story object',1,'[{\"added\": {}}]',1,1),(127,'2017-06-11 20:32:25','51','Story object',2,'[{\"changed\": {\"fields\": [\"cover_loc\"]}}]',1,1),(128,'2017-06-11 20:40:38','52','Story object',2,'[{\"changed\": {\"fields\": [\"story_title\", \"writer\", \"brief\", \"storyfile\", \"Originally_published\", \"Genre\"]}}]',1,1),(129,'2017-06-11 20:41:46','52','Story object',2,'[{\"changed\": {\"fields\": [\"collection\"]}}]',1,1),(130,'2017-06-11 20:45:05','52','Story object',2,'[{\"changed\": {\"fields\": [\"cover_loc\"]}}]',1,1),(131,'2017-06-11 20:46:14','52','Story object',2,'[{\"changed\": {\"fields\": [\"cover_loc\"]}}]',1,1),(132,'2017-06-11 20:46:52','52','Story object',2,'[{\"changed\": {\"fields\": [\"writer\"]}}]',1,1),(133,'2017-06-11 20:52:46','53','Story object',1,'[{\"added\": {}}]',1,1),(134,'2017-06-11 21:14:58','54','Story object',1,'[{\"added\": {}}]',1,1),(135,'2017-06-11 21:16:24','54','Story object',2,'[]',1,1),(136,'2017-06-11 21:17:43','54','Story object',2,'[{\"changed\": {\"fields\": [\"storyfile\"]}}]',1,1),(137,'2017-06-11 21:21:11','54','Story object',2,'[]',1,1),(138,'2017-06-11 21:24:41','54','Story object',2,'[{\"changed\": {\"fields\": [\"storyfile\"]}}]',1,1),(139,'2017-06-11 21:49:36','55','Story object',1,'[{\"added\": {}}]',1,1),(140,'2017-06-11 21:55:38','55','Story object',2,'[{\"changed\": {\"fields\": [\"collection\", \"cover_loc\", \"read_count\", \"storyfile\"]}}]',1,1),(141,'2017-06-11 22:17:53','56','Story object',1,'[{\"added\": {}}]',1,1),(142,'2017-06-11 22:32:38','23','Story object',2,'[{\"changed\": {\"fields\": [\"storyfile\"]}}]',1,1),(143,'2017-06-11 22:35:44','5','Story object',2,'[{\"changed\": {\"fields\": [\"storyfile\"]}}]',1,1),(144,'2017-06-11 22:36:48','5','Story object',2,'[{\"changed\": {\"fields\": [\"storyfile\"]}}]',1,1),(145,'2017-06-11 22:37:19','5','Story object',2,'[{\"changed\": {\"fields\": [\"storyfile\"]}}]',1,1),(146,'2017-06-12 05:20:07','57','Story object',1,'[{\"added\": {}}]',1,1),(147,'2017-06-13 09:58:04','39','Story object',2,'[{\"changed\": {\"fields\": [\"published\", \"Genre\", \"Adaptations\"]}}]',1,1),(148,'2017-06-13 10:00:20','43','Story object',2,'[{\"changed\": {\"fields\": [\"published\", \"Genre\", \"Adaptations\"]}}]',1,1),(149,'2017-06-13 10:01:05','42','Story object',2,'[{\"changed\": {\"fields\": [\"published\", \"Originally_published\", \"Genre\", \"Adaptations\"]}}]',1,1),(150,'2017-06-13 10:01:28','41','Story object',2,'[{\"changed\": {\"fields\": [\"published\", \"Originally_published\", \"Genre\", \"Adaptations\"]}}]',1,1),(151,'2017-06-13 10:01:56','40','Story object',2,'[{\"changed\": {\"fields\": [\"published\", \"Originally_published\", \"Genre\", \"Adaptations\"]}}]',1,1),(152,'2017-06-13 10:06:42','44','Story object',2,'[{\"changed\": {\"fields\": [\"published\"]}}]',1,1),(153,'2017-06-13 10:06:51','43','Story object',2,'[]',1,1),(154,'2017-06-14 17:11:48','1','Story object',2,'[{\"changed\": {\"fields\": [\"cover_loc\"]}}]',1,1),(155,'2017-06-14 17:12:02','2','Story object',2,'[{\"changed\": {\"fields\": [\"cover_loc\"]}}]',1,1),(156,'2017-06-14 17:12:13','3','Story object',2,'[{\"changed\": {\"fields\": [\"cover_loc\"]}}]',1,1),(157,'2017-06-14 17:12:28','4','Story object',2,'[{\"changed\": {\"fields\": [\"cover_loc\"]}}]',1,1),(158,'2017-06-14 17:15:41','5','Story object',2,'[{\"changed\": {\"fields\": [\"cover_loc\"]}}]',1,1),(159,'2017-06-14 17:15:55','6','Story object',2,'[{\"changed\": {\"fields\": [\"cover_loc\"]}}]',1,1),(160,'2017-06-14 17:24:33','8','Story object',2,'[{\"changed\": {\"fields\": [\"cover_loc\"]}}]',1,1),(161,'2017-06-14 17:28:25','7','Story object',2,'[{\"changed\": {\"fields\": [\"cover_loc\"]}}]',1,1),(162,'2017-06-14 17:34:31','9','Story object',2,'[{\"changed\": {\"fields\": [\"cover_loc\"]}}]',1,1),(163,'2017-06-14 17:38:12','14','Story object',2,'[{\"changed\": {\"fields\": [\"cover_loc\"]}}]',1,1),(164,'2017-06-14 17:43:11','18','Story object',2,'[{\"changed\": {\"fields\": [\"cover_loc\"]}}]',1,1),(165,'2017-06-14 17:45:59','19','Story object',2,'[{\"changed\": {\"fields\": [\"cover_loc\"]}}]',1,1),(166,'2017-06-14 17:58:32','27','Story object',2,'[{\"changed\": {\"fields\": [\"cover_loc\", \"brief\"]}}]',1,1),(167,'2017-06-14 18:04:25','29','Story object',2,'[{\"changed\": {\"fields\": [\"cover_loc\"]}}]',1,1),(168,'2017-06-14 18:17:04','30','Story object',2,'[{\"changed\": {\"fields\": [\"cover_loc\"]}}]',1,1),(169,'2017-06-14 18:20:01','30','Story object',2,'[{\"changed\": {\"fields\": [\"cover_loc\"]}}]',1,1),(170,'2017-06-14 19:12:26','58','Story object',1,'[{\"added\": {}}]',1,1),(171,'2017-06-14 19:46:51','58','Story object',2,'[{\"changed\": {\"fields\": [\"slug\"]}}]',1,1),(172,'2017-06-14 20:23:31','59','Story object',1,'[{\"added\": {}}]',1,1),(173,'2017-06-14 20:25:36','59','Story object',2,'[{\"changed\": {\"fields\": [\"storyfile\"]}}]',1,1),(174,'2017-06-14 20:26:43','59','Story object',2,'[{\"changed\": {\"fields\": [\"storyfile\"]}}]',1,1),(175,'2017-06-14 20:42:51','60','Story object',1,'[{\"added\": {}}]',1,1),(176,'2017-06-14 20:46:54','60','Story object',2,'[{\"changed\": {\"fields\": [\"slug\"]}}]',1,1),(177,'2017-06-14 21:03:31','61','Story object',1,'[{\"added\": {}}]',1,1),(178,'2017-06-14 21:03:45','61','Story object',2,'[{\"changed\": {\"fields\": [\"slug\"]}}]',1,1),(179,'2017-06-14 21:05:16','61','Story object',2,'[{\"changed\": {\"fields\": [\"cover_loc\"]}}]',1,1),(180,'2017-06-14 21:07:04','61','Story object',2,'[]',1,1),(181,'2017-06-14 21:08:52','61','Story object',2,'[]',1,1),(182,'2017-06-14 21:10:58','61','Story object',2,'[]',1,1),(183,'2017-06-14 21:12:59','61','Story object',2,'[{\"changed\": {\"fields\": [\"storyfile\"]}}]',1,1),(184,'2017-06-14 21:14:54','61','Story object',2,'[{\"changed\": {\"fields\": [\"storyfile\"]}}]',1,1),(185,'2017-07-07 02:46:08','1','Story object',2,'[{\"changed\": {\"fields\": [\"cover_loc\"]}}]',1,1),(186,'2017-07-07 02:47:16','1','Story object',2,'[{\"changed\": {\"fields\": [\"cover_loc\"]}}]',1,1),(187,'2017-07-07 02:48:10','1','Story object',2,'[{\"changed\": {\"fields\": [\"cover_loc\"]}}]',1,1),(188,'2017-07-07 02:48:43','1','Story object',2,'[{\"changed\": {\"fields\": [\"cover_loc\"]}}]',1,1),(189,'2017-07-07 02:49:29','2','Story object',2,'[{\"changed\": {\"fields\": [\"cover_loc\"]}}]',1,1),(190,'2017-07-07 02:50:55','3','Story object',2,'[{\"changed\": {\"fields\": [\"cover_loc\"]}}]',1,1),(191,'2017-07-07 02:51:46','4','Story object',2,'[{\"changed\": {\"fields\": [\"cover_loc\"]}}]',1,1),(192,'2017-07-07 02:54:14','5','Story object',2,'[{\"changed\": {\"fields\": [\"cover_loc\"]}}]',1,1),(193,'2017-07-07 02:55:22','6','Story object',2,'[{\"changed\": {\"fields\": [\"cover_loc\"]}}]',1,1),(194,'2017-07-07 03:01:03','12','Story object',2,'[{\"changed\": {\"fields\": [\"cover_loc\"]}}]',1,1),(195,'2017-07-07 03:03:05','12','Story object',2,'[{\"changed\": {\"fields\": [\"cover_loc\"]}}]',1,1),(196,'2017-07-07 03:07:19','8','Story object',2,'[{\"changed\": {\"fields\": [\"cover_loc\"]}}]',1,1),(197,'2017-07-07 03:14:01','15','Story object',2,'[{\"changed\": {\"fields\": [\"cover_loc\"]}}]',1,1),(198,'2017-07-07 03:15:36','16','Story object',2,'[{\"changed\": {\"fields\": [\"cover_loc\"]}}]',1,1),(199,'2017-07-07 07:15:26','62','Story object',1,'[{\"added\": {}}]',1,1),(200,'2017-07-07 07:20:52','62','Story object',2,'[{\"changed\": {\"fields\": [\"slug\"]}}]',1,1),(201,'2017-07-07 10:27:11','26','Story object',2,'[{\"changed\": {\"fields\": [\"cover_loc\"]}}]',1,1),(202,'2017-07-07 10:28:30','25','Story object',2,'[{\"changed\": {\"fields\": [\"cover_loc\"]}}]',1,1),(203,'2017-07-07 10:29:18','25','Story object',2,'[{\"changed\": {\"fields\": [\"cover_loc\"]}}]',1,1),(204,'2017-07-07 10:34:22','25','Story object',2,'[{\"changed\": {\"fields\": [\"cover_loc\"]}}]',1,1),(205,'2017-07-07 10:40:38','10','Story object',2,'[{\"changed\": {\"fields\": [\"cover_loc\"]}}]',1,1),(206,'2017-07-07 10:43:19','20','Story object',2,'[{\"changed\": {\"fields\": [\"cover_loc\"]}}]',1,1),(207,'2017-07-07 10:55:16','23','Story object',2,'[{\"changed\": {\"fields\": [\"cover_loc\"]}}]',1,1),(208,'2017-07-07 10:57:08','11','Story object',2,'[{\"changed\": {\"fields\": [\"cover_loc\"]}}]',1,1),(209,'2017-07-08 19:50:28','24','Story object',2,'[{\"changed\": {\"fields\": [\"writer\", \"collection\", \"cover_loc\", \"brief\", \"Originally_published\"]}}]',1,1),(210,'2017-07-08 19:59:13','17','Story object',2,'[{\"changed\": {\"fields\": [\"story_title\", \"writer\", \"cover_loc\", \"brief\", \"Originally_published\", \"Genre\", \"Adaptations\"]}}]',1,1),(211,'2017-07-08 20:00:04','17','Story object',2,'[{\"changed\": {\"fields\": [\"story_title\"]}}]',1,1),(212,'2017-07-08 20:00:39','17','Story object',2,'[{\"changed\": {\"fields\": [\"story_title\"]}}]',1,1),(213,'2017-07-08 20:02:06','17','Story object',2,'[{\"changed\": {\"fields\": [\"writer\"]}}]',1,1),(214,'2017-07-08 20:04:11','13','Story object',2,'[{\"changed\": {\"fields\": [\"cover_loc\"]}}]',1,1),(215,'2017-07-15 03:52:23','30','Story object',2,'[{\"changed\": {\"fields\": [\"cover_loc\"]}}]',1,1),(216,'2017-07-16 06:05:35','62','Story object',2,'[{\"changed\": {\"fields\": [\"cover_loc\"]}}]',1,1),(217,'2017-07-16 06:06:38','61','Story object',2,'[{\"changed\": {\"fields\": [\"cover_loc\"]}}]',1,1),(218,'2017-07-16 06:08:16','59','Story object',2,'[{\"changed\": {\"fields\": [\"cover_loc\"]}}]',1,1),(219,'2017-07-16 06:08:41','31','Story object',2,'[{\"changed\": {\"fields\": [\"cover_loc\"]}}]',1,1),(220,'2017-07-16 06:09:45','59','Story object',2,'[{\"changed\": {\"fields\": [\"cover_loc\"]}}]',1,1),(221,'2017-07-16 06:12:53','30','Story object',2,'[{\"changed\": {\"fields\": [\"cover_loc\"]}}]',1,1),(222,'2017-07-16 06:14:31','12','Story object',2,'[{\"changed\": {\"fields\": [\"cover_loc\"]}}]',1,1),(223,'2017-07-16 06:15:21','8','Story object',2,'[{\"changed\": {\"fields\": [\"cover_loc\"]}}]',1,1),(224,'2017-07-16 06:16:47','3','Story object',2,'[{\"changed\": {\"fields\": [\"cover_loc\"]}}]',1,1),(225,'2017-07-16 06:18:06','20','Story object',2,'[{\"changed\": {\"fields\": [\"cover_loc\"]}}]',1,1),(226,'2017-07-16 06:19:10','15','Story object',2,'[{\"changed\": {\"fields\": [\"cover_loc\"]}}]',1,1),(227,'2017-07-16 06:20:09','23','Story object',2,'[{\"changed\": {\"fields\": [\"cover_loc\"]}}]',1,1),(228,'2017-07-16 06:22:03','57','Story object',2,'[{\"changed\": {\"fields\": [\"cover_loc\"]}}]',1,1),(229,'2017-07-16 06:22:41','47','Story object',2,'[{\"changed\": {\"fields\": [\"cover_loc\"]}}]',1,1),(230,'2017-07-16 06:23:45','6','Story object',2,'[{\"changed\": {\"fields\": [\"cover_loc\"]}}]',1,1),(231,'2017-07-16 06:24:35','50','Story object',2,'[{\"changed\": {\"fields\": [\"cover_loc\"]}}]',1,1),(232,'2017-07-16 06:25:15','32','Story object',2,'[{\"changed\": {\"fields\": [\"cover_loc\"]}}]',1,1),(233,'2017-07-16 06:26:47','13','Story object',2,'[{\"changed\": {\"fields\": [\"cover_loc\"]}}]',1,1),(234,'2017-07-16 06:30:32','46','Story object',2,'[{\"changed\": {\"fields\": [\"cover_loc\"]}}]',1,1),(235,'2017-07-16 06:31:15','5','Story object',2,'[{\"changed\": {\"fields\": [\"cover_loc\"]}}]',1,1),(236,'2017-07-16 06:31:57','44','Story object',2,'[{\"changed\": {\"fields\": [\"cover_loc\"]}}]',1,1),(237,'2017-07-16 06:32:38','4','Story object',2,'[{\"changed\": {\"fields\": [\"cover_loc\"]}}]',1,1),(238,'2017-07-16 06:33:02','1','Story object',2,'[{\"changed\": {\"fields\": [\"cover_loc\"]}}]',1,1),(239,'2017-07-16 06:33:47','11','Story object',2,'[{\"changed\": {\"fields\": [\"story_title\", \"cover_loc\"]}}]',1,1),(240,'2017-07-16 06:34:19','7','Story object',2,'[{\"changed\": {\"fields\": [\"cover_loc\"]}}]',1,1),(241,'2017-07-16 06:34:48','11','Story object',2,'[]',1,1),(242,'2017-07-16 06:35:05','22','Story object',2,'[{\"changed\": {\"fields\": [\"cover_loc\"]}}]',1,1),(243,'2017-07-16 06:35:33','27','Story object',2,'[{\"changed\": {\"fields\": [\"cover_loc\"]}}]',1,1),(244,'2017-07-16 06:36:11','14','Story object',2,'[{\"changed\": {\"fields\": [\"cover_loc\"]}}]',1,1),(245,'2017-07-16 06:36:35','45','Story object',2,'[{\"changed\": {\"fields\": [\"cover_loc\"]}}]',1,1),(246,'2017-07-16 06:38:41','29','Story object',2,'[{\"changed\": {\"fields\": [\"cover_loc\"]}}]',1,1),(247,'2017-07-16 06:41:29','33','Story object',2,'[{\"changed\": {\"fields\": [\"cover_loc\", \"Genre\", \"Adaptations\"]}}]',1,1),(248,'2017-07-16 06:41:59','34','Story object',2,'[{\"changed\": {\"fields\": [\"cover_loc\", \"Genre\", \"Adaptations\"]}}]',1,1),(249,'2017-07-16 06:43:52','35','Story object',2,'[{\"changed\": {\"fields\": [\"cover_loc\", \"Genre\", \"Adaptations\"]}}]',1,1),(250,'2017-07-16 06:47:00','36','Story object',2,'[{\"changed\": {\"fields\": [\"cover_loc\", \"Genre\", \"Adaptations\"]}}]',1,1),(251,'2017-07-16 06:49:42','37','Story object',2,'[{\"changed\": {\"fields\": [\"cover_loc\", \"Genre\", \"Adaptations\"]}}]',1,1),(252,'2017-07-16 06:50:24','38','Story object',2,'[{\"changed\": {\"fields\": [\"cover_loc\", \"Genre\", \"Adaptations\"]}}]',1,1),(253,'2017-07-16 06:52:32','39','Story object',2,'[{\"changed\": {\"fields\": [\"cover_loc\"]}}]',1,1),(254,'2017-07-16 06:52:47','40','Story object',2,'[{\"changed\": {\"fields\": [\"cover_loc\"]}}]',1,1),(255,'2017-07-16 06:55:02','40','Story object',2,'[]',1,1),(256,'2017-07-16 06:55:20','41','Story object',2,'[{\"changed\": {\"fields\": [\"cover_loc\"]}}]',1,1),(257,'2017-07-16 06:56:31','42','Story object',2,'[{\"changed\": {\"fields\": [\"cover_loc\"]}}]',1,1),(258,'2017-07-16 06:58:54','43','Story object',2,'[{\"changed\": {\"fields\": [\"cover_loc\"]}}]',1,1),(259,'2017-07-16 07:01:05','2','Story object',2,'[{\"changed\": {\"fields\": [\"cover_loc\"]}}]',1,1),(260,'2017-07-16 07:02:37','55','Story object',2,'[{\"changed\": {\"fields\": [\"cover_loc\"]}}]',1,1),(261,'2017-07-16 07:03:24','54','Story object',2,'[{\"changed\": {\"fields\": [\"cover_loc\"]}}]',1,1),(262,'2017-07-16 07:04:56','49','Story object',2,'[{\"changed\": {\"fields\": [\"cover_loc\"]}}]',1,1),(263,'2017-07-16 07:07:03','52','Story object',2,'[{\"changed\": {\"fields\": [\"cover_loc\"]}}]',1,1),(264,'2017-07-16 07:07:37','10','Story object',2,'[{\"changed\": {\"fields\": [\"cover_loc\"]}}]',1,1),(265,'2017-07-16 07:08:14','25','Story object',2,'[{\"changed\": {\"fields\": [\"cover_loc\"]}}]',1,1),(266,'2017-07-16 07:08:38','18','Story object',2,'[{\"changed\": {\"fields\": [\"cover_loc\"]}}]',1,1),(267,'2017-07-16 07:09:16','17','Story object',2,'[{\"changed\": {\"fields\": [\"cover_loc\"]}}]',1,1),(268,'2017-07-16 07:10:07','31','Story object',2,'[]',1,1),(269,'2017-07-16 07:10:45','9','Story object',2,'[{\"changed\": {\"fields\": [\"cover_loc\"]}}]',1,1),(270,'2017-07-16 07:11:11','58','Story object',2,'[{\"changed\": {\"fields\": [\"cover_loc\"]}}]',1,1),(271,'2017-07-16 07:11:52','24','Story object',2,'[{\"changed\": {\"fields\": [\"story_title\", \"cover_loc\"]}}]',1,1),(272,'2017-07-16 07:12:19','19','Story object',2,'[{\"changed\": {\"fields\": [\"cover_loc\"]}}]',1,1),(273,'2017-07-16 07:13:18','60','Story object',2,'[{\"changed\": {\"fields\": [\"cover_loc\"]}}]',1,1),(274,'2017-07-16 07:13:58','16','Story object',2,'[{\"changed\": {\"fields\": [\"cover_loc\"]}}]',1,1),(275,'2017-07-16 07:14:28','26','Story object',2,'[{\"changed\": {\"fields\": [\"cover_loc\"]}}]',1,1),(276,'2017-07-16 07:15:40','21','Story object',2,'[{\"changed\": {\"fields\": [\"cover_loc\"]}}]',1,1),(277,'2017-07-16 07:19:13','28','Story object',2,'[{\"changed\": {\"fields\": [\"cover_loc\", \"read_count\"]}}]',1,1),(278,'2017-07-16 07:21:02','51','Story object',2,'[{\"changed\": {\"fields\": [\"cover_loc\"]}}]',1,1),(279,'2017-07-16 07:22:36','48','Story object',2,'[{\"changed\": {\"fields\": [\"cover_loc\"]}}]',1,1),(280,'2017-07-16 07:23:30','61','Story object',2,'[]',1,1),(281,'2017-07-16 07:31:33','50','Story object',2,'[{\"changed\": {\"fields\": [\"cover_loc\"]}}]',1,1),(282,'2017-07-16 10:12:07','53','Story object',2,'[{\"changed\": {\"fields\": [\"cover_loc\"]}}]',1,1),(283,'2017-07-16 10:15:06','56','Story object',2,'[{\"changed\": {\"fields\": [\"cover_loc\"]}}]',1,1),(284,'2017-07-30 13:22:21','63','Story object',1,'[{\"added\": {}}]',1,1),(285,'2017-07-30 13:24:41','63','Story object',2,'[{\"changed\": {\"fields\": [\"slug\"]}}]',1,1),(286,'2017-07-30 14:08:57','64','Story object',1,'[{\"added\": {}}]',1,1),(287,'2017-07-30 14:10:14','64','Story object',2,'[{\"changed\": {\"fields\": [\"slug\"]}}]',1,1),(288,'2017-07-30 14:52:45','64','Story object',2,'[]',1,1),(289,'2017-07-30 14:58:02','64','Story object',2,'[{\"changed\": {\"fields\": [\"cover_loc\"]}}]',1,1),(290,'2017-07-30 16:48:51','65','Story object',1,'[{\"added\": {}}]',1,1),(291,'2017-07-30 16:50:03','65','Story object',2,'[{\"changed\": {\"fields\": [\"writer\", \"collection\", \"slug\"]}}]',1,1),(292,'2017-07-30 16:57:57','65','Story object',2,'[]',1,1),(293,'2017-07-30 16:58:40','65','Story object',2,'[{\"changed\": {\"fields\": [\"storyfile\"]}}]',1,1),(294,'2017-07-30 18:41:40','66','Story object',1,'[{\"added\": {}}]',1,1),(295,'2017-07-30 18:41:56','66','Story object',2,'[{\"changed\": {\"fields\": [\"slug\"]}}]',1,1),(296,'2017-08-01 01:01:23','67','Story object',1,'[{\"added\": {}}]',1,1),(297,'2017-08-01 01:02:18','67','Story object',2,'[{\"changed\": {\"fields\": [\"slug\"]}}]',1,1),(298,'2017-08-01 01:04:05','67','Story object',2,'[{\"changed\": {\"fields\": [\"story_title\", \"brief\"]}}]',1,1),(299,'2017-08-02 02:28:55','68','Story object',1,'[{\"added\": {}}]',1,1),(300,'2017-08-02 02:29:42','68','Story object',2,'[{\"changed\": {\"fields\": [\"slug\"]}}]',1,1),(301,'2017-09-02 11:28:19','69','Story object',1,'[{\"added\": {}}]',1,1),(302,'2017-09-02 11:28:51','69','Story object',2,'[{\"changed\": {\"fields\": [\"slug\"]}}]',1,1),(303,'2017-09-02 11:42:05','69','Story object',2,'[{\"changed\": {\"fields\": [\"brief\"]}}]',1,1);
/*!40000 ALTER TABLE `django_admin_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_content_type`
--

DROP TABLE IF EXISTS `django_content_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `django_content_type_app_label_76bd3d3b_uniq` (`app_label`,`model`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_content_type`
--

LOCK TABLES `django_content_type` WRITE;
/*!40000 ALTER TABLE `django_content_type` DISABLE KEYS */;
INSERT INTO `django_content_type` VALUES (2,'admin','logentry'),(5,'auth','group'),(4,'auth','permission'),(3,'auth','user'),(6,'contenttypes','contenttype'),(1,'personal','story'),(13,'personal','uservotes'),(7,'sessions','session'),(11,'social_django','association'),(9,'social_django','code'),(8,'social_django','nonce'),(12,'social_django','partial'),(10,'social_django','usersocialauth');
/*!40000 ALTER TABLE `django_content_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_migrations`
--

DROP TABLE IF EXISTS `django_migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_migrations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `applied` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_migrations`
--

LOCK TABLES `django_migrations` WRITE;
/*!40000 ALTER TABLE `django_migrations` DISABLE KEYS */;
INSERT INTO `django_migrations` VALUES (1,'contenttypes','0001_initial','2017-05-04 03:19:11'),(2,'auth','0001_initial','2017-05-04 03:19:13'),(3,'admin','0001_initial','2017-05-04 03:19:13'),(4,'admin','0002_logentry_remove_auto_add','2017-05-04 03:19:13'),(5,'contenttypes','0002_remove_content_type_name','2017-05-04 03:19:14'),(6,'auth','0002_alter_permission_name_max_length','2017-05-04 03:19:14'),(7,'auth','0003_alter_user_email_max_length','2017-05-04 03:19:14'),(8,'auth','0004_alter_user_username_opts','2017-05-04 03:19:14'),(9,'auth','0005_alter_user_last_login_null','2017-05-04 03:19:14'),(10,'auth','0006_require_contenttypes_0002','2017-05-04 03:19:14'),(11,'auth','0007_alter_validators_add_error_messages','2017-05-04 03:19:14'),(12,'auth','0008_alter_user_username_max_length','2017-05-04 03:19:15'),(13,'personal','0001_initial','2017-05-04 03:19:15'),(14,'sessions','0001_initial','2017-05-04 03:19:15'),(15,'personal','0002_story_brief','2017-05-04 18:13:23'),(16,'personal','0003_auto_20170610_0623','2017-06-10 06:23:28'),(20,'personal','0004_story_slug','2017-06-13 22:31:01'),(21,'personal','0005_auto_20170613_2143','2017-06-13 22:31:01'),(22,'personal','0006_auto_20170614_1924','2017-06-14 19:24:42'),(23,'personal','0007_auto_20170614_1930','2017-06-14 19:30:24'),(24,'personal','0008_auto_20170614_1932','2017-06-14 19:32:13'),(25,'personal','0009_auto_20170624_0438','2017-06-25 05:11:26'),(26,'default','0001_initial','2017-06-25 05:13:34'),(27,'social_auth','0001_initial','2017-06-25 05:13:34'),(28,'default','0002_add_related_name','2017-06-25 05:13:34'),(29,'social_auth','0002_add_related_name','2017-06-25 05:13:34'),(30,'default','0003_alter_email_max_length','2017-06-25 05:13:34'),(31,'social_auth','0003_alter_email_max_length','2017-06-25 05:13:34'),(32,'default','0004_auto_20160423_0400','2017-06-25 05:13:34'),(33,'social_auth','0004_auto_20160423_0400','2017-06-25 05:13:34'),(34,'social_auth','0005_auto_20160727_2333','2017-06-25 05:13:34'),(35,'social_django','0006_partial','2017-06-25 05:13:59'),(36,'social_django','0002_add_related_name','2017-06-25 05:13:59'),(37,'social_django','0005_auto_20160727_2333','2017-06-25 05:13:59'),(38,'social_django','0003_alter_email_max_length','2017-06-25 05:13:59'),(39,'social_django','0004_auto_20160423_0400','2017-06-25 05:13:59'),(40,'social_django','0001_initial','2017-06-25 05:13:59'),(41,'personal','0010_auto_20170808_1932','2017-08-08 19:32:39');
/*!40000 ALTER TABLE `django_migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_session`
--

DROP TABLE IF EXISTS `django_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime NOT NULL,
  PRIMARY KEY (`session_key`),
  KEY `django_session_de54fa62` (`expire_date`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_session`
--

LOCK TABLES `django_session` WRITE;
/*!40000 ALTER TABLE `django_session` DISABLE KEYS */;
INSERT INTO `django_session` VALUES ('1gv8inhnlo47p7us7tptl50lre9so24r','MTBmMDM4YWVmYTcwNWY4YWFlMzllYzdkOTc5ZDc1ZjE4MzQ0ZmE4NDp7Il9hdXRoX3VzZXJfaWQiOiI1IiwiZmFjZWJvb2tfc3RhdGUiOiJpSnBFRVVvTEd5Z3FYNkdVN0p3WFJDeWlOMlNEbElwSiIsInNvY2lhbF9hdXRoX2xhc3RfbG9naW5fYmFja2VuZCI6ImZhY2Vib29rIiwidWlkIjoiMTQ1ODI4ODMzNzU4MzU4OSIsIl9hdXRoX3VzZXJfYmFja2VuZCI6InNvY2lhbF9jb3JlLmJhY2tlbmRzLmZhY2Vib29rLkZhY2Vib29rT0F1dGgyIiwiX2F1dGhfdXNlcl9oYXNoIjoiNTNiYWRjOTlhOTc2YzYzNGExZjZhMmJiNDdjMWVlYjI4M2ZlNjBkNiJ9','2017-09-10 07:27:48'),('4ea7rf16coc67m2i9krt0coyhhoplcru','ZDFiOTFjMzZlYjYzYTg3N2RkMzlmNWY3OTViZjVjMjdmYjIwMmFiNDp7ImZhY2Vib29rX3N0YXRlIjoic3RQTzRQVlhET1R6NGNrakZRamlmbDYzSnd2cGwxNDEifQ==','2017-09-19 00:48:23'),('4f8sewn4dg0hvk2ymnyhvha0as0s3sbu','ZjllZTVmMWJjOWY3YTk1ZWIwOGEwOGY0MGNhMTk2MWFlYTRhNTIzODp7ImZhY2Vib29rX3N0YXRlIjoibjBlN1JrN0tlTldKMFhPOThVblpMbmhQU1lBWTlUTTcifQ==','2017-08-29 04:45:11'),('65k5iom6vzm0jqezbpc9zjjrsmtddb6y','M2U1ODIyMzZmMjJkN2I2YmY4YTMyNzdjNzE1YTllMjYxNDM0OGIwNTp7Il9hdXRoX3VzZXJfaGFzaCI6IjBhMjE0MTEwMzQ1ODM4OGMzZWQ1ODc2OThjNGYwMmZjOTk4YTk2YmIiLCJ1aWQiOiIxMDIxMTUxMzAyNTg3NTM2NiIsInNvY2lhbF9hdXRoX2xhc3RfbG9naW5fYmFja2VuZCI6ImZhY2Vib29rIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoic29jaWFsX2NvcmUuYmFja2VuZHMuZmFjZWJvb2suRmFjZWJvb2tPQXV0aDIiLCJfYXV0aF91c2VyX2lkIjoiMSIsImZhY2Vib29rX3N0YXRlIjoic2w2NE9LRWNWeXRkZWZLQ3ZMcFdxbVlhSUFrOVVjTXEifQ==','2017-08-27 14:35:42'),('80ctjaeapxq1x84cvr09s3j5645eskwl','ZDBjNjcwNzM3ODhkYTVjZDIxNTA4YzUyZWI3ZTMzMjU5MWFmN2I0NDp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9oYXNoIjoiMGEyMTQxMTAzNDU4Mzg4YzNlZDU4NzY5OGM0ZjAyZmM5OThhOTZiYiIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=','2017-06-23 20:46:48'),('8adjpigfinxrgt2m253vvo8y6a8gu1qa','NzYyZGUzYTk2MjE2ZmQ5MGU1Y2ZiYjIzNDhlODlkOGY4OWZiMmE2Njp7ImZhY2Vib29rX3N0YXRlIjoiQldUdWpMcWxhWUF3UkJMZVR4eTJlWlBacnRCYUs3TlcifQ==','2017-08-19 03:17:48'),('8bn3st3zv1zfewa8kj5qzdae5am0bm2t','ZGQ1OGI2OTRhYzEyMTMzNjI3YjkyNzNmNTQwMTYxNjBhOWFjM2E2MDp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiIyZTVjNjhkZGEwYmU3NWE0OTUxY2NjYmFjNzZjNTBiNDlhOTRmZGNhIn0=','2017-06-20 16:08:48'),('8jxq8jwwquwgzkb69novyxj5bvf2opok','MzJlZDVjODJkZjk5NWRlMzEzNTlmYWQzYzBkZjkzYWQ4NzY3OWY1Mzp7Il9hdXRoX3VzZXJfaGFzaCI6IjBhMjE0MTEwMzQ1ODM4OGMzZWQ1ODc2OThjNGYwMmZjOTk4YTk2YmIiLCJ1aWQiOiIxMDIxMTUxMzAyNTg3NTM2NiIsInNvY2lhbF9hdXRoX2xhc3RfbG9naW5fYmFja2VuZCI6ImZhY2Vib29rIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoic29jaWFsX2NvcmUuYmFja2VuZHMuZmFjZWJvb2suRmFjZWJvb2tPQXV0aDIiLCJfYXV0aF91c2VyX2lkIjoiMSIsImZhY2Vib29rX3N0YXRlIjoic2RybGF0S3BsWkFqSnA2eWxSa3NUdFF1NnFKY3JJRXcifQ==','2017-08-27 13:42:24'),('9phirx4x1jhskyu6gi4uvz21vqjma75f','YzRiY2JkZjQxZDI0Nzk2N2Q1N2M3NGE3ZWU3YWMxNDYxNGI0ZGYwMDp7InNvY2lhbF9hdXRoX2xhc3RfbG9naW5fYmFja2VuZCI6ImZhY2Vib29rIiwidWlkIjoiMTAyMTE1MTMwMjU4NzUzNjYiLCJmYWNlYm9va19zdGF0ZSI6InhyaXhhMENSd2VVdWVzcW41ZG85aTZPNHVLRjREQzdWIiwiX2F1dGhfdXNlcl9pZCI6IjEiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJzb2NpYWxfY29yZS5iYWNrZW5kcy5mYWNlYm9vay5GYWNlYm9va09BdXRoMiIsIl9hdXRoX3VzZXJfaGFzaCI6IjBhMjE0MTEwMzQ1ODM4OGMzZWQ1ODc2OThjNGYwMmZjOTk4YTk2YmIifQ==','2017-07-20 06:10:37'),('a11f4ri16gzelbvy02o95hlsf7smnip4','ODlhOGFhNDI2ODVkMTlhNGIxMzQ1NDY2Njk5NGM5MmM0ZGY5MDZjMDp7ImZhY2Vib29rX3N0YXRlIjoiUHQyUFlWeGNlVVNWMUUwR2JSNTRJRkU2QUl6SE9JbW0ifQ==','2017-08-30 14:44:32'),('bj9olj91yewqn1gnie84fgxbcqs06iwc','YmE4ZmY2NDljYWNlNjQyMDE1NmIwMzJlNWFjZTU1NzY2ZGU2NzAyODp7ImZhY2Vib29rX3N0YXRlIjoibDRaZjJhRFhSOFBBTTRzRFFQdzRIcmswWEw3UzhRYnEifQ==','2017-07-22 17:28:07'),('bsedieiscbj7q0opa8mpq1kwvyri5b6l','NTYxMmI1NTQ5NDZjYzYwNzNmYzIwYWJjZDNhZjEwZGFkYmQyZWFmMjp7Il9hdXRoX3VzZXJfaGFzaCI6IjBhMjE0MTEwMzQ1ODM4OGMzZWQ1ODc2OThjNGYwMmZjOTk4YTk2YmIiLCJfYXV0aF91c2VyX2lkIjoiMSIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=','2017-07-30 06:03:30'),('cu8r8f38zauss7iieigiinwyj53jejrj','NmVkMWM5MDc3YWZhMTMwYWFiNjcxMjdmOTUzOTRjZTAzNzc1MmRkZDp7ImZhY2Vib29rX3N0YXRlIjoieElSd0Q1RUQ1TDc3bGNvWG9DYklxSXRDWDI5V3B6aEUifQ==','2017-08-30 17:34:19'),('dmkzbgm28vtamsbneajhjqldvx7ievb0','OGU3NDBmMWE1MTYxYzg5YjNiMTAzN2E5NGRhZWQ2MzA2OGU5MTYyNDp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9oYXNoIjoiMGEyMTQxMTAzNDU4Mzg4YzNlZDU4NzY5OGM0ZjAyZmM5OThhOTZiYiIsImZhY2Vib29rX3N0YXRlIjoiTlRGY0dDNXpadndKdU5GS3N3eklNV2RrUVdhdlNUVHYiLCJ1aWQiOiIxMDIxMTUxMzAyNTg3NTM2NiIsInNvY2lhbF9hdXRoX2xhc3RfbG9naW5fYmFja2VuZCI6ImZhY2Vib29rIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoic29jaWFsX2NvcmUuYmFja2VuZHMuZmFjZWJvb2suRmFjZWJvb2tPQXV0aDIifQ==','2017-08-01 18:12:31'),('e8jzknl28g80b3r2bzdzva0k89lewf1b','YWUyNDZiOWUwMDk2NTU2MjQyMmNhYTgyMGZkOGQzYzA2NzVjYjgwZTp7ImZhY2Vib29rX3N0YXRlIjoiV1FJaE90SWhTQmdtWVBDOVh2TnVzMWNuV0NNTDRRN3EifQ==','2017-07-09 09:23:18'),('fy6idljliy68vryixwubpxbs2vpjqpko','ZDU5ZmVmY2YyNGE0OGIyZmUyZTZkOGE5YTY2Yzk2NmY1YjQyYjE2MTp7InNvY2lhbF9hdXRoX2xhc3RfbG9naW5fYmFja2VuZCI6ImZhY2Vib29rIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoic29jaWFsX2NvcmUuYmFja2VuZHMuZmFjZWJvb2suRmFjZWJvb2tPQXV0aDIiLCJfYXV0aF91c2VyX2lkIjoiNCIsIl9hdXRoX3VzZXJfaGFzaCI6ImZiMGIzZmJjMjlmNTZiZjY0ZjM2NDA4OGVlYWUzZjZlOGNkOGVjMzUiLCJmYWNlYm9va19zdGF0ZSI6ImlrNE5PUlJWZzVLVnc0dlYxOE9ndjRld0lWd2lubVZiIiwidWlkIjoiMTAyMDc2Mjg2NjI1MTQwMDEifQ==','2017-07-10 08:57:05'),('g9bwu5c72a3fr3dionaco6d5fb332lhb','ZDMzMGQwM2FlNWRhZDFjMDNjZjcxYzczOGE3ZjA4NzIwY2I5Y2IwNjp7Il9hdXRoX3VzZXJfaGFzaCI6IjBhMjE0MTEwMzQ1ODM4OGMzZWQ1ODc2OThjNGYwMmZjOTk4YTk2YmIiLCJzb2NpYWxfYXV0aF9sYXN0X2xvZ2luX2JhY2tlbmQiOiJmYWNlYm9vayIsIl9hdXRoX3VzZXJfYmFja2VuZCI6InNvY2lhbF9jb3JlLmJhY2tlbmRzLmZhY2Vib29rLkZhY2Vib29rT0F1dGgyIiwidWlkIjoiMTAyMTE1MTMwMjU4NzUzNjYiLCJfYXV0aF91c2VyX2lkIjoiMSIsImZhY2Vib29rX3N0YXRlIjoiRGE1ZmNJMHRhaU1IQ0xLd2lubThPUGFpc2tmWmJ4UFcifQ==','2017-09-04 09:23:08'),('ghsxkjisepmqum69hexdduwahdmzwspl','YmEzYTcyMzg4NmZiY2U3YmE5MmVlN2YwYzFhYTE1ZDFiNzc0MjY5Zjp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiZmFjZWJvb2tfc3RhdGUiOiJScURhTDhCa29jeVlCcHlWTWw0aDJ0VG44UmFoU1FpOSIsInNvY2lhbF9hdXRoX2xhc3RfbG9naW5fYmFja2VuZCI6ImZhY2Vib29rIiwidWlkIjoiMTAyMTE1MTMwMjU4NzUzNjYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJzb2NpYWxfY29yZS5iYWNrZW5kcy5mYWNlYm9vay5GYWNlYm9va09BdXRoMiIsIl9hdXRoX3VzZXJfaGFzaCI6IjBhMjE0MTEwMzQ1ODM4OGMzZWQ1ODc2OThjNGYwMmZjOTk4YTk2YmIifQ==','2017-09-10 05:46:37'),('hqhkghfjm78w5s3mjtrfxl2xy6juaufj','MmE1YWMzODIzNzdhNzU3OGU3NzhjYjkxMzA0NjVkMWM2Y2RmOTZkMzp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9oYXNoIjoiMGEyMTQxMTAzNDU4Mzg4YzNlZDU4NzY5OGM0ZjAyZmM5OThhOTZiYiIsImZhY2Vib29rX3N0YXRlIjoiTzF3TFNSb0FIb0pjajJlTFE2TFlrckw4UXk3QVJUekUiLCJ1aWQiOiIxMDIxMTUxMzAyNTg3NTM2NiIsInNvY2lhbF9hdXRoX2xhc3RfbG9naW5fYmFja2VuZCI6ImZhY2Vib29rIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoic29jaWFsX2NvcmUuYmFja2VuZHMuZmFjZWJvb2suRmFjZWJvb2tPQXV0aDIifQ==','2017-08-01 04:59:55'),('icacyl7t15j4r4qmh5ioepw8jhxn5323','NGRlNTNkNjI4NWY4OGYzYzlmZWVjNTdjZmExZmQ3MDUxYjAyNTU3ZDp7Il9hdXRoX3VzZXJfaWQiOiIzIiwiZmFjZWJvb2tfc3RhdGUiOiJQcDRsdXA3dTNTRkd6MDY0NGgxQVhGNEQzc0F6aGNXeCIsInNvY2lhbF9hdXRoX2xhc3RfbG9naW5fYmFja2VuZCI6ImZhY2Vib29rIiwidWlkIjoiMTUyNTIzNjc2MDg2MTM4NyIsIl9hdXRoX3VzZXJfYmFja2VuZCI6InNvY2lhbF9jb3JlLmJhY2tlbmRzLmZhY2Vib29rLkZhY2Vib29rT0F1dGgyIiwiX2F1dGhfdXNlcl9oYXNoIjoiMTY4YzdhN2ZjZGQwNDhjYjIxM2QyNjdmNGVhMmIzZGE2OWM0NWYxZCJ9','2017-09-11 11:25:22'),('ifsllyeaxu0g70tahm6qgl75pntozyx7','NzRlMTBlYjJkNjQwMWZkYTc3NGVhZjc4NTFmZjEzNTI4OWJhNWQxMzp7Il9hdXRoX3VzZXJfaWQiOiIzIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoic29jaWFsX2NvcmUuYmFja2VuZHMuZmFjZWJvb2suRmFjZWJvb2tPQXV0aDIiLCJzb2NpYWxfYXV0aF9sYXN0X2xvZ2luX2JhY2tlbmQiOiJmYWNlYm9vayIsIl9hdXRoX3VzZXJfaGFzaCI6IjE2OGM3YTdmY2RkMDQ4Y2IyMTNkMjY3ZjRlYTJiM2RhNjljNDVmMWQiLCJmYWNlYm9va19zdGF0ZSI6IjgwOUlZRHdiTXhJeklOQzdIRUNKRnY3SEVxWVBGZjRvIn0=','2017-07-10 07:53:06'),('k92r7r8iy20gkdex8dif46glusdh5la1','YmFhMzVkM2M2MjAyZWQ0MjVmZGQzOWM0ZGJiZDA4ODJmZGUwZTZkMDp7Il9hdXRoX3VzZXJfaWQiOiIzIiwiX2F1dGhfdXNlcl9oYXNoIjoiMTY4YzdhN2ZjZGQwNDhjYjIxM2QyNjdmNGVhMmIzZGE2OWM0NWYxZCIsImZhY2Vib29rX3N0YXRlIjoieFF6MDQzZGUyNVVnblBlbktycVc0bFZGNmF6cEI3TnYiLCJ1aWQiOiIxNTI1MjM2NzYwODYxMzg3Iiwic29jaWFsX2F1dGhfbGFzdF9sb2dpbl9iYWNrZW5kIjoiZmFjZWJvb2siLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJzb2NpYWxfY29yZS5iYWNrZW5kcy5mYWNlYm9vay5GYWNlYm9va09BdXRoMiJ9','2017-07-31 14:53:23'),('kgl9mvkj3bl2r0co0ehijgcitqzsiwh8','ZGQ1OGI2OTRhYzEyMTMzNjI3YjkyNzNmNTQwMTYxNjBhOWFjM2E2MDp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiIyZTVjNjhkZGEwYmU3NWE0OTUxY2NjYmFjNzZjNTBiNDlhOTRmZGNhIn0=','2017-05-18 03:40:28'),('m9809y12qgwdderxs6szw14vqodfvfsj','ZDBjNjcwNzM3ODhkYTVjZDIxNTA4YzUyZWI3ZTMzMjU5MWFmN2I0NDp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9oYXNoIjoiMGEyMTQxMTAzNDU4Mzg4YzNlZDU4NzY5OGM0ZjAyZmM5OThhOTZiYiIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=','2017-06-26 05:08:01'),('mqcnuw414b79wqzgolm9o79zorwm2ys9','MmY2ODM5NDc2M2I0NmUxN2JjN2NiMTFjM2E3M2JiOWYwYjljMzI0Mjp7ImZhY2Vib29rX3N0YXRlIjoia2Z5ZXJGUG93TnVnN2NaUEFQZ1F0Y0IyQWxuckEyYzgifQ==','2017-09-20 11:59:55'),('mxzo5fspfubffbzh7a04pndqfxoy6ic2','MWViMGI0YTQxZGU0YjljMzE2MzdkNGVkODljMWMzNWJhNWQ4ZWI2MDp7ImZhY2Vib29rX3N0YXRlIjoieWNYcGtWU3dIVHJES0JkbWNNUXRmU25kaFhDSEEwODIifQ==','2017-09-05 21:37:16'),('n51sa4svnp449i1hjzjqhbo488l5y8ea','ZGQ1OGI2OTRhYzEyMTMzNjI3YjkyNzNmNTQwMTYxNjBhOWFjM2E2MDp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiIyZTVjNjhkZGEwYmU3NWE0OTUxY2NjYmFjNzZjNTBiNDlhOTRmZGNhIn0=','2017-05-20 11:08:37'),('oq0mbdugd1vvamd9erlpl215qri6b8ct','NjY4MjA4ZTM2MTJlYjFjMjZmMTViNTMzZGY1YjdjOTViMGRjMmJmZDp7ImZhY2Vib29rX3N0YXRlIjoicFRiNjN2bFl1cWFIMmROeG03VG9EYjdsTHp0Vllzc3kiLCJfYXV0aF91c2VyX2lkIjoiMSIsInVpZCI6IjEwMjExNTEzMDI1ODc1MzY2Iiwic29jaWFsX2F1dGhfbGFzdF9sb2dpbl9iYWNrZW5kIjoiZmFjZWJvb2siLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJzb2NpYWxfY29yZS5iYWNrZW5kcy5mYWNlYm9vay5GYWNlYm9va09BdXRoMiIsIl9hdXRoX3VzZXJfaGFzaCI6IjBhMjE0MTEwMzQ1ODM4OGMzZWQ1ODc2OThjNGYwMmZjOTk4YTk2YmIifQ==','2017-08-13 07:26:21'),('osajthvtv7kkw6dnyl0g0ne9ve3qpr4d','ZWJkMDg1OWYyNDc3YjMwNWUyOGM1MGRhOWY2YTIxOGQyOGQwZDc2YTp7ImZhY2Vib29rX3N0YXRlIjoiN0hyZk9VaDhubmlWUmVjOVE3OXdGN01hbzFvSU1rb2MifQ==','2017-08-29 04:19:15'),('ox0l07xfoeo94ba4ymackmbi8jrgjohp','MTYwNDhmN2MwZGUwMjZkNmYyMGJjOWI4ZjRlNThiN2JhYTE3MWJlNjp7ImZhY2Vib29rX3N0YXRlIjoiM0JnbDVxSEFqZ0JvZUUyYUtHbG5PZGJmU0tiSlVKZ2siLCJuZXh0IjoiLyJ9','2017-07-09 08:08:09'),('r5xzuf1g29pb3gddelnos9oris8c0nv1','NTRiZmY4YWU1ZDYyZmFkMTA1OWZmZTVjZTdkZjY4MTEzZDIzMTEzMzp7ImZhY2Vib29rX3N0YXRlIjoidWNJaUkwYVoyVFkzcVVhZXhCUHRzWmFqRjRSbnhRNnYifQ==','2017-07-26 02:22:54'),('rkhe1j79zvjnaxhywbbzdkoog86dclp7','ODhiNmI5YzRkM2RkNTYxMWQwOTIwOWIxNjgwNzUzODliNmY3YjVlMDp7Il9hdXRoX3VzZXJfaGFzaCI6IjBhMjE0MTEwMzQ1ODM4OGMzZWQ1ODc2OThjNGYwMmZjOTk4YTk2YmIiLCJzb2NpYWxfYXV0aF9sYXN0X2xvZ2luX2JhY2tlbmQiOiJmYWNlYm9vayIsIl9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoic29jaWFsX2NvcmUuYmFja2VuZHMuZmFjZWJvb2suRmFjZWJvb2tPQXV0aDIiLCJmYWNlYm9va19zdGF0ZSI6IndldTJGdE9Cdk9nN2VCMFVyWU5YR3BpS3lZaklVMzAzIn0=','2017-08-01 04:59:48'),('rqi9h51offseug0vr03iq2f3cehile6t','OWI2NmU1NjU3NzUzZTNjODZjODg5YTdlODVhMmJkMjgwZGZhMzE3Nzp7ImZhY2Vib29rX3N0YXRlIjoiRGUxV0VDSno3T2kyYVlkRzlXOVUwSEp0d2lMRlowR1AifQ==','2017-08-16 10:21:25'),('s2zjqwm98j34vc41mtlbbbydg1e2tzh0','NjdlNjRjMjQ5YzVkZDc0MmQwOGM1N2EzOGIzZmY5ZjRiYjEzNmFlYzp7ImZhY2Vib29rX3N0YXRlIjoiWkdZSTAwaFV2UzIwUjUwbW5LMTZPUjc2UVhmWHFGY2oifQ==','2017-08-26 03:52:06'),('sfksalktsovye2d6k4su5473brltlgcz','NzYyYjVjOWM2YjU2NTkxMzQ0ZDI1NzdjZGY4NWIxN2ZmMDZlMmZjZTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiIwYTIxNDExMDM0NTgzODhjM2VkNTg3Njk4YzRmMDJmYzk5OGE5NmJiIn0=','2017-07-17 06:00:02'),('sqq968xtdmbrmpa0ul9sztcnkw8iyo0z','OTlmNzI1NTZhM2ZjY2E0NDhiMWUyNGRhMzA4ZTgzYzY0NmExZjNlMTp7ImZhY2Vib29rX3N0YXRlIjoiSTU4dkJiZ1FyYkF0cnRHWlRWWTJyWnBwWFZjdWp5eEQifQ==','2017-08-11 11:54:41'),('tq5y69m9p7dt6sez6ukjvz17dzys5nyh','MWIwOTNhMzMwOWRmYjM1ODUzZTcxN2E1ZjA1YTkzOTQzNDViNGU1Zjp7InNvY2lhbF9hdXRoX2xhc3RfbG9naW5fYmFja2VuZCI6ImZhY2Vib29rIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoic29jaWFsX2NvcmUuYmFja2VuZHMuZmFjZWJvb2suRmFjZWJvb2tPQXV0aDIiLCJfYXV0aF91c2VyX2lkIjoiMSIsIl9hdXRoX3VzZXJfaGFzaCI6IjBhMjE0MTEwMzQ1ODM4OGMzZWQ1ODc2OThjNGYwMmZjOTk4YTk2YmIiLCJmYWNlYm9va19zdGF0ZSI6IjQ3NG5LUWFOTG5IVnZaZTdtRDJ0S09WMnpiNlp6ZXVZIiwidWlkIjoiMTAyMTE1MTMwMjU4NzUzNjYifQ==','2017-07-10 08:48:56'),('vi1wdclsq4bod5icx1ujklnxrbo2zho6','NjliMzJiOTRmNThmOGE4MjBlMjY5OWQzMTJkMmIxOWVjMzQ5ZWEzZjp7ImZhY2Vib29rX3N0YXRlIjoic29qelVaM1ZFVVZwaThURDhSTWlnS2ExUEhDUDVWVzMifQ==','2017-09-07 06:45:24'),('wlq20n0f7vxzdq6fip6dc66ttlgyqrra','YjYwYTBjMmM1NDQzMGM3ODZlZmJkYWJlMTgyMTkxMjYzMjQ5YTQxNTp7ImZhY2Vib29rX3N0YXRlIjoiVUhGZUk0UXpYcjZSdDdSYzNTeUZkbmFvbHZJTnNoa1IifQ==','2017-07-11 19:01:40'),('wxcqqfjdii9mfmc5tp0qu0i9fo32hveu','OGFjMzdlYWM3OTc3MzlmNzYzNWNiMzdhZTFjMzdmZmIzYWUxN2I5YTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6InNvY2lhbF9jb3JlLmJhY2tlbmRzLmZhY2Vib29rLkZhY2Vib29rT0F1dGgyIiwiX2F1dGhfdXNlcl9pZCI6IjEiLCJzb2NpYWxfYXV0aF9sYXN0X2xvZ2luX2JhY2tlbmQiOiJmYWNlYm9vayIsIl9hdXRoX3VzZXJfaGFzaCI6IjBhMjE0MTEwMzQ1ODM4OGMzZWQ1ODc2OThjNGYwMmZjOTk4YTk2YmIiLCJmYWNlYm9va19zdGF0ZSI6ImZLSXRWTTNCcDc1Z3g1ZzlLNXAwZ21ZQVNmSU1tcXpGIiwidWlkIjoiMTAyMTE1MTMwMjU4NzUzNjYifQ==','2017-08-27 05:36:23'),('xkm4sszkap0zkdqrfjh5ml6zmnfn21lb','YWY3ZWI5NjhmYzdjMjQ1N2IzMTc4ZTUyYjU1MjFlOWVkYzM1OTljYzp7ImZhY2Vib29rX3N0YXRlIjoid0RtQlI2cG9pdTZqZkpsVlREWnlkNTFqdmtiYURvbjUifQ==','2017-07-30 10:49:29'),('yqbx2weh6i9l8xr61qztlhqpe16z3x0g','ZGQ1OGI2OTRhYzEyMTMzNjI3YjkyNzNmNTQwMTYxNjBhOWFjM2E2MDp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiIyZTVjNjhkZGEwYmU3NWE0OTUxY2NjYmFjNzZjNTBiNDlhOTRmZGNhIn0=','2017-05-20 10:42:50'),('z55h6bu605uj9m0q1i2k1sgg2oi0249p','OWIwNWJmZDQ4MmQ1MDMwNDcxNTI2N2JlMDcxY2I2ZWJlNjM4Mjk0NDp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjEiLCJfYXV0aF91c2VyX2hhc2giOiIwYTIxNDExMDM0NTgzODhjM2VkNTg3Njk4YzRmMDJmYzk5OGE5NmJiIn0=','2017-09-16 11:22:59'),('zu104vay6aa3ks0pcx6n2lb7e5pglrt6','Yzg1MjVhZjgxNWM0ZjFkOThiYTQ1MmFkNzdhNTUwYzIxYjRmNDMxODp7Il9hdXRoX3VzZXJfaGFzaCI6IjE2OGM3YTdmY2RkMDQ4Y2IyMTNkMjY3ZjRlYTJiM2RhNjljNDVmMWQiLCJmYWNlYm9va19zdGF0ZSI6IkxhNm1Cd1FwcEpRcXQyOGNFemZRQXVsY1FXdXRmWUgxIiwic29jaWFsX2F1dGhfbGFzdF9sb2dpbl9iYWNrZW5kIjoiZmFjZWJvb2siLCJfYXV0aF91c2VyX2lkIjoiMyIsIl9hdXRoX3VzZXJfYmFja2VuZCI6InNvY2lhbF9jb3JlLmJhY2tlbmRzLmZhY2Vib29rLkZhY2Vib29rT0F1dGgyIiwidWlkIjoiMTUyNTIzNjc2MDg2MTM4NyJ9','2017-07-25 09:21:33');
/*!40000 ALTER TABLE `django_session` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `personal_story`
--

DROP TABLE IF EXISTS `personal_story`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `personal_story` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `story_title` varchar(200) DEFAULT NULL,
  `published` datetime NOT NULL,
  `writer` varchar(200) NOT NULL,
  `collection` varchar(200) NOT NULL,
  `cover_loc` varchar(400) NOT NULL,
  `likes` int(11) NOT NULL,
  `dislikes` int(11) NOT NULL,
  `read_count` int(11) NOT NULL,
  `storyfile` varchar(250) NOT NULL,
  `brief` varchar(250) NOT NULL,
  `Adaptations` varchar(250) NOT NULL,
  `Genre` varchar(100) NOT NULL,
  `Original_language` varchar(50) NOT NULL,
  `Originally_published` varchar(250) NOT NULL,
  `awards` varchar(250) NOT NULL,
  `type` varchar(100) NOT NULL,
  `slug` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `personal_story_slug_17dd59a4_uniq` (`slug`),
  KEY `personal_story_2dbcba41` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=70 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `personal_story`
--

LOCK TABLES `personal_story` WRITE;
/*!40000 ALTER TABLE `personal_story` DISABLE KEYS */;
INSERT INTO `personal_story` VALUES (1,'A Case Of Identity','2017-05-04 04:42:00','Sir Arthur Conan Doyle','The Adventures of Sherlock Holmes','CaseofIdentity2002_lozhni',0,0,986,'A_Case_of_Identity.txt','Sutherland wants Holmes to find the location of one \"Mr. Hosmer Angel\" (Identity.18). Sutherland has heard of Holmes\'s all-around awesomeness and is pretty mad because her stepfather..','None','Short story, Detective fiction, Crime Fiction, Noir fiction','English','1891','None','Story','a-case-of-identity-1'),(2,'A Scandal in Bohemia','2017-05-04 05:00:37','Arthur Conan Doyle','The Adventures of Sherlock Holmes','scandal_in_bohemia',0,0,618,'A_SCANDAL_IN_BOHEMIA.txt','Sherlock Holmes finds himself evenly matched when he is employed by the King of Bohemia to retrieve an indiscreet photograph from the American actress and singer, Irene Adler','None','Short story, Detective fiction, Crime Fiction, Noir fiction','English','1891','None','Story','a-scandal-in-bohemia-2'),(3,'Adventure of  The  Blue Carbuncle','2017-05-04 05:02:17','Sir Arthur Conan Doyle','The Adventures of Sherlock Holmes','Adventure_Of_The_Blue_Carbuncle',0,0,313,'ADVENTURE_OF_THE_BLUE_CARBUNCLE.txt','When the Countess of Morcar\'s priceless blue carbuncle is stolen, a reformed thief is charged with the crime.','None','Short story, Detective fiction, Crime Fiction, Noir fiction','English','1891','None','Story','adventure-of-the-blue-carbuncle-3'),(4,'The Boscombe Valley Mystery','2017-05-04 05:05:43','Arthur Conan Doyle','The Adventures of Sherlock Holmes','9780071273855_ymx4kh',0,0,360,'The_Boscombe_Valley_Mystery.txt','\"Boscombe Valley is a country district not very far from Ross, in Herefordshire.\" There is no real location matching this description....','None','Short story, Detective fiction, Crime Fiction, Noir fiction','English','None','None','Story','the-boscombe-valley-mystery-4'),(5,'Tthe Five Orange Pips','2017-05-06 11:30:08','Sir Arthur Conan Doyle','The Adventures of Sherlock Holmes','22979548._UY200__rodqlj',0,0,78,'THE_MAN_WITH_THE_TWISTED_LIP.txt','Watson finds a case file from 1887, which was solved only according to Holmes\'s logic, and has never been definitively proven.','None','Short story, Detective fiction, Crime Fiction, Noir fiction','English','1891','None','Story','tthe-five-orange-pips-5'),(6,'THE MAN WITH THE TWISTED LIP','2017-05-06 11:35:08','Sir Arthur Conan Doyle','The Adventures of Sherlock Holmes','themanwihtwistedlip',0,0,91,'THE_MAN_WITH_THE_TWISTED_LIP.txt','Responding to a grieving wife, Holmes investigates the apparent murder of her husband in an apartment above an opium den.','None','Short story, Detective fiction, Crime Fiction, Noir fiction','English','1891','None','Story','the-man-with-the-twisted-lip-6'),(7,'THE STORY OF ALADDIN','2017-05-06 11:42:55','Edward William Lane, Muhsin Mahdi','The Arabian Nights','aladdin_lwxaw8',0,0,86,'aladdin.txt','The story of Aladin tells how a poor boy Aladin got a magic lamp which changed the way of his life completely.','None','Arabic literature,Fantasy ,fiction,Fantasy fiction','English','1704','None','Story','the-story-of-aladdin-7'),(8,'BEAUTY AND THE BEAST','2017-05-06 11:47:27','unanonymous','Unknown','beautyandbeast',0,0,157,'beautyandbeast.txt','The three daughters were all handsome, but particularly the youngest; indeed, she was so very beautiful, that in her childhood every one called her the Little Beauty..','None','Children\'s,Fantasy','English','None','None','Story','beauty-and-the-beast-8'),(9,'ONE EYE, TWO EYES, THREE EYES','2017-05-06 11:51:34','Collected by Brothers Grimm','The Green Fairy Book','oneeye_iwhkno',0,0,82,'threesisters.txt','A woman had three daughters: The eldest had one eye in the middle of her forehead, the second had two eyes like ordinary people, the third had three eyes...','Lee Drapp wrote an adapted version called \"The Story of One Eye, Two Eye, and Three Eye\" (2016), illustrated by Saraid Claxton','Children\'s,Fantasy','German','None','None','Story','one-eye-two-eyes-three-eyes-9'),(10,'BLUE BEARD','2017-05-06 11:54:00','Charles Perrault','Unknown','images_3_ruyddg',0,0,120,'bluebeard.txt','The tale tells the story of a wealthy violent man in the habit of murdering his wives and the attempts of one wife to avoid the fate of her predecessors.','None','Fairy tale','English','1657','None','Story','blue-beard-10'),(11,'THE BREMEN TOWN MUSICIANS','2017-05-06 11:56:54','Brothers Grimm','Unknown','brementown_cover_df00ft',0,0,77,'bremenrownmusicians.txt','A donkey, a dog, a cat, and a rooster (or hen), all past their prime years in life and usefulness, decide to go to Bremen, known for its freedom, to live without owners and become musicians','None','Children\'s','German','1819','None','Story','the-bremen-town-musicians-11'),(12,'CINDERELLA','2017-05-06 00:00:00','Colleced by Charles Perrault','Histoires ou contes du temps passé','Cindrellastory',0,0,70,'cindrella.txt','Once upon a time, there was a kind girl named Cinderella. All of the animals loved her, especially two mice named Gus and Jaq...','http://www.imdb.com/title/tt1661199/','Children\'s,Fantasy,Fairytale,Folktale','English','1697','None','Story','cinderella-12'),(13,'THE FAIR ONE WITH GOLDEN LOCKS','2017-05-06 12:20:11','Madame d\'Aulnoy','Fairy Tales Every Child Should Know','faironewithgoldenlocks',0,0,45,'faironewithgoldenlocks.txt','The story of the beautiful daughter of a king, whom they called The Fair One with Golden Locks','None','Children\'s,Fairytale,Folktale','English','None','None','Story','the-fair-one-with-golden-locks-13'),(14,'THE GOLDEN GOOSE','2017-05-06 12:21:07','collected by the Brothers Grimm','Unknown','goldengoose_eupwon',0,0,49,'goldengoose.txt','There was a man who had three sons, the youngest of whom was called Dummling,[*] and was despised, mocked, and sneered at on every occasion...','None','Children\'s,Fantasy','English','1812','None','Story','the-golden-goose-14'),(15,'GRACIOSA AND PERCINET','2017-05-06 12:23:41','Madame d\'Aulnoy','The Red Fairy Book,','GRACIOSA_AND_PERCINET',0,0,74,'graciosaandprecient.txt','ONCE upon a time there lived a King and Queen who had one charming daughter. She was so graceful and pretty and clever that she was called Graciosa, and the Queen was so fond of her that she could think of nothing else...','None','Children\'s,Fantasy','French','1890','None','Story','graciosa-and-percinet-15'),(16,'SNOW-WHITE AND ROSE-RED','2017-05-06 12:31:31','collected by the Brothers Grimm','Grimm\'s Fairy Tale Classics','snow_white_and_rose_red_by_melzayas-d53afc5_di60ip',0,0,45,'snowwhite.txt','She had two children who were like the two rose-trees, and one was called Snow-white and the other Rose-red. They were as good and happy, as busy and ...','None','Children\'s,Fantasy,Fairytale','German','1911','None','Story','snow-white-and-rose-red-16'),(17,'Snow White and THE MAGIC MIRROR','2017-05-06 15:14:53','The Brothers Grimm','Unknown','MV5BMTYyMzI2MTE2Ml5BMl5BanBnXkFtZTcwODgyMzgxMQ_._V1_UX182_CR0_0_182_268_AL__ymvkwb',0,0,30,'magicmirror.txt','Once upon a time, there was a beautiful princess named Snow White. She was kind and gentle and a friend to all animals. One day, Snow White met a charming ...','None','FairyTale,Fiction,Children\'s','English','1812','None','Story','snow-white-and-the-magic-mirror-17'),(18,'THE NOTHING EQUATION','2017-05-06 15:16:01','Tom Godwin','Unknown','nothingequation_tdxrit',0,0,27,'nothingequation.txt','THE cruiser vanished back into hyperspace and he was alone in the observation bubble, ten thousand light-years beyond the galaxy\'s outermost sun...','None','Sci Fi Classic.sci-fi,Science fiction','English','None','None','Story','the-nothing-equation-18'),(19,'A PAIR OF SILK STOCKINGS','2017-05-06 15:17:36','Kate Chopin','Unknown','pairofsilkstokings_jeqvjp',0,0,32,'pairofsilkstockings.txt','Little Mrs. Sommers one day found herself the unexpected possessor of fifteen dollars. It seemed to her a very large amount of money, ...','None','Short story','English','September 1897','None','Story','a-pair-of-silk-stockings-19'),(20,'THE PRINCE WITH NOSE','2017-05-06 15:19:07','Dinah Maria Mulock.','The Little Lame Prince.London','littleprincewithnose',0,0,53,'princewithnose.txt','The king went to a good fairy to inquire what he should do. ... but she will be obliged to marry any person who is adroit enough to walk upon the cat\'s tail. ...','None','Children\'s,Fantasy','English','1875','None','Story','the-prince-with-nose-20'),(21,'PUSS IN BOOTS','2017-05-06 15:20:42','Giovanni Francesco Straparola','The Facetious Nights of Straparola','Puss-in-boots-book_exscwp',0,0,26,'pussinboots.txt','fairy tale about a cat who uses trickery and deceit to gain power, wealth, and the hand of a princess in marriage for his penniless and low-born master.','None','Children\'s,Fantasy','English','1550–53','None','Story','puss-in-boots-21'),(22,'LITTLE RED-RIDING-HOOD','2017-05-06 15:22:09','Charles Perrault','Italian Folktales','Little_Red_Riding_Hood_Meeting_the_Wolf_w4sg9m',0,0,19,'redridinghood.txt','The story revolves around a girl called Little Red Riding Hood, who walks through the woods to deliver food to her sickly grandmother..','None','Children\'s,Fantasy,Fairytale,Folktale','European','10th Century','None','Story','little-red-riding-hood-22'),(23,'THE SECOND VOYAGE OF SINDBAD THE SAILOR','2017-05-06 15:24:26','unanonymous','The Arabian Nights','sinbadthesailor',0,0,34,'sinbad2.txt','Sindbad , Accidentally abandoned by his shipmates again, he finds himself stranded in an island which contains roc eggs...','None','Arabic literature,Fantasy ,fiction,Fantasy fiction','English','1637','None','Story','the-second-voyage-of-sindbad-the-sailor-23'),(24,'SLEEPING BEAUTY IN THE WOOD','2017-05-06 15:26:02','CHARLES PERRAULT','None','perrault-sleeping-beauty-crane-en_cctzfl',0,0,49,'sleepingbeauty.txt','Once upon a time a king and a queen lived, but they couldn’t have children. They were so sad it can’t be put in words. They visited doctors and tried everything to conceive a child.  But, a miracle happened, and they’ve had a baby girl.','None','Children\'s,Fantasy,Science Fiction','English','1870','None','Story','sleeping-beauty-in-the-wood-24'),(25,'THE TELL-TALE HEART','2017-05-06 15:28:44','Edgar Allan Poe','Unknown','images_4_mjqjrr',0,0,28,'telltaleheart.txt','iT\'s TRue! yes, i have been ill, very ill. But why do you say that. I have lost control of my mind, why do you say that I am mad? Can you not ...','None','Genres:','English','1843','None','Story','the-tell-tale-heart-25'),(26,'THE FROG-PRINCE','2017-05-06 15:30:12','collected by the Brothers Grimm','Unknown','product_thumbnail_ofsn9u',0,0,26,'thefrogprince.txt','A spoiled princess reluctantly befriends the Frog Prince (meeting him after dropping a gold ball into a pond), who magically transforms into a handsome prince','None','Children\'s,Fairytale,Folktale','German','2005','None','Story','the-frog-prince-26'),(27,'The Gift of MAGI','2017-05-06 15:32:05','O. Henry','The Four Million','giftofmagi_j9usiv',0,0,55,'thegiftofmagi.txt','A husband and wife sacrifice treasured possessions in order to buy each other Christmas presents.','None','Short story,Lovestory','English','None','None','Story','the-gift-of-magi-27'),(28,'THE THREE BEARS','2017-05-06 15:34:47','Robert Southey','Unknown','9788498711271_zsixn7',0,0,33,'threebears.txt','In a far-off country there was once a little girl who was called Silver-hair, because her curly hair shone brightly. She was a sad romp, and so restless..','None','Children\'s,Fantasy','English','1837','None','Story','the-three-bears-28'),(29,'THE TWELVE BROTHERS','2017-05-06 15:37:00','Brothers Grimm','The Red Fairy Book','grimmstails_dahai9',0,0,39,'twelvebrothers','A king wants to kill his twelve sons, so that if his thirteenth child would be a girl, she alone can inherit his kingdom. The Queen tells this to their youngest son, Benjamin,..','None','Children\'s,Fantasy,Fairytale','English','None','None','Story','the-twelve-brothers-29'),(30,'THE UGLY DUCKLING','2017-05-06 15:38:11','Hans Christian Andersen','Unknown','1uglyduckling_j6iycw',0,0,19,'uglyduckling.txt','It was so beautiful out on the country, it was summer- the wheat fields were golden, the oats were green, and down among the green meadows the hay was ...','None','Fairy tale, Fiction, Children\'s literature, Picture book','English, Danish','11 November 1843','None','Story','the-ugly-duckling-30'),(31,'THE YELLOW DWARF','2017-05-06 15:39:40','Madame d\'Aulnoy, adapted by  Andrew Lang','The Blue Fairy Book','WalterCrane_yellowdwarf-cover_zutpa5',0,0,21,'yellowdwarf.txt','A widowed queen spoiled her only daughter, who was so beautiful that kings vied for the honor of her hand, not believing they could attain it. Uneasy that her daughter would never marry, the queen went to visit the Fairy of the Desert for advice..','None','Children\'s,Fantasy,Fairytale','French','None','None','Story','the-yellow-dwarf-31'),(32,'The Time Machine','2017-06-10 11:30:08','H. G. (Herbert George) Wells','he Time Machine','5102696976_d815a1314b_b_ja89xd',0,0,80,'thetimemachine.txt','A dreamer obsessed with traveling through time builds himself a time machine and, much to his surprise, travels ...','None','Science Fiction','English','1895','None','Story','the-time-machine-32'),(33,'THE CRYSTAL EGG','2017-06-10 11:47:27','H. G. WELLS','Tales of Space and Time','8436725._UY200__yerpa8',0,0,27,'Thecrystalegg.txt','The story tells of a shop owner, named Mr. Cave, who finds a strange crystal egg that serves as a window into the planet Mars.','None','fiction','English','None','None','Story','the-crystal-egg-33'),(34,'Story of the Stone Age','2017-06-10 11:42:55','H. G. WELLS','Tales of Space and Time','Story-of-the-Stone-Age_dqv7qw',0,0,38,'storyofstoneage.txt','Andoo, the huge cave bear, who lived in the cave up the gorge, had never even seen a man in all his wise and respectable life, until midway ..','None','fiction','English','None','None','Story','story-of-the-stone-age-34'),(35,'THe Star','2017-06-10 11:35:08','H. G. WELLS','Tales of Space and Time','a0d55eb92cf1dc97bbd76550975f750d_qo70h6',0,0,38,'thefirsthorseman.txt','It was on the first day of the New Year that the announcement was made, almost simultaneously from three observatories, that the motion of the planet ...','None','fiction','English','None','None','Story','the-star-35'),(36,'THE MAN WHO COULD WORK MIRACLES','2017-06-10 11:51:34','H. G. WELLS','Tales of Space and Time','manwhocoulddomiracle_zjysti',0,0,15,'TheManWhoCouldWorkMiracles.txt','An ordinary man suddenly finds that anything he says comes true. Or at least, almost anything!','None','fiction','English','None','None','Story','the-man-who-could-work-miracles-36'),(37,'THE COUNTRY OF THE BLIND','2017-05-06 11:54:00','H. G. WELLS','Unknown','3912167_t1dze5',0,0,51,'THE_COUNTRY_OF_THE_BLIND.txt','Three hundred miles and more from Chimborazo, one hundred from the snows of Cotopaxi, in the wildest wastes of Ecuador\'s Andes, there lies that mysterious mountain valley, cut off from all the world of men ...','None','fiction','English','None','None','Story','the-country-of-the-blind-37'),(38,'A DREAM OF ARMAGEDDON','2017-06-10 11:54:00','H. G. WELLS','British weekly magazine','598_ma1b5n',0,0,16,'A_DREAM_OF_ARMAGEDDON.txt','The man with the white face entered the carriage at Rugby. He moved slowly in spite of the urgency of his porter, and even while he ...','None','fiction','English','None','None','Story','a-dream-of-armageddon-38'),(39,'THE RED ROOM','2017-06-10 11:54:00','H. G. WELLS','The Idler magazine','51kqFbrDe9L._SY346__ftvxu7',0,0,29,'THE_RED_ROOM.txt','A short gothic story written by H. G. Wells in 1894. It was first published in the March 1896 edition of The Idler magazine.','None','Fiction','English','None','None','Story','the-red-room-39'),(40,'The Diamond Maker','2017-06-13 00:00:00','H. G. WELLS','Pall Mall Budget','26102219._UY200__ezcksk',0,0,21,'The_Diamond_Maker.txt','Some business had detained me in Chancery Lane nine in the evening, and thereafter, having some inkling of a headache, I was ...','None','Fantasy,ShortStory','English','1894-01-01','None','Story','the-diamond-maker-40'),(41,'THE CONE','2017-06-13 00:00:00','H. G. WELLS','UNICORN','468_the_cone_-_hg_wells_thb_etn8nw',0,0,13,'THE_CONE.txt','The night was hot and overcast, the sky red, rimmed with the lingering sunset of mid-summer. They sat at the open window, trying to fancy the air was ..','None','Fantasy,ShortStory','English','1895','None','Story','the-cone-41'),(42,'THE TRUTH ABOUT PYECRAFT','2017-06-13 00:00:00','H. G. WELLS','The Strand Magazine','The-Truth-about-Pyecraft-H-SDL584250957-1-fce12_aarjje',0,0,22,'THE_TRUTH_ABOUT_PYECRAFT.txt','He sits not a dozen yards away. If I glance over my shoulder I can see him. And if I catch his eye--and usually I catch his eye-- it meets ...','None','Fantasy,ShortStory','English','1903','None','Story','the-truth-about-pyecraft-42'),(43,'The Sea Raiders','2017-06-09 00:00:00','H. G. WELLS','short stories by Wells','9781496199102-us_kenpa5',0,0,21,'The_Sea_Raiders.txt','The Sea Raiders\" is a short story by H. G. Wells, first published in 1896 in The Weekly Sun Literary Supplement. It was included in The Plattner Story and Others, a collection of short stories by Wells published by Methuen & Co. in 1897.','None','Fantasy','English','None','None','Story','the-sea-raiders-43'),(44,'The Picture of Dorian Gray','2017-06-12 00:00:00','Oscar Wilde','BOOK','22433734869_dd7f90bcdc_b_x8ghvt',0,0,20,'The_Picture_of_Dorian_Gray','The Picture of Dorian Gray is a philosophical novel by Oscar Wilde, first published complete in the July 1890 issue of Lippincott\'s Monthly Magazine','http://www.imdb.com/title/tt1235124/','Philosophical fiction','English','1890','None','Book','the-picture-of-dorian-gray-44'),(45,'THE JUNGLEBOOK.','2017-06-10 00:00:00','Rudyard Kipling','BOOK','The_Jungle_Book__1910__cover_iglbik',0,0,76,'THE_JUNGLEBOOK.txt','The Jungle Book is a collection of stories by English author Rudyard Kipling. The stories are fables, using animals in an anthropomorphic manner to give moral lessons.','None','Children\'s,Fantasy','English','1894','None','Book','the-junglebook-45'),(46,'Frankestein','2017-06-10 00:00:00','Mary Shelley','BOOK','frankenstein',0,0,150,'Frankestein','young scientist who creates a grotesque but sapient creature in an unorthodox scientific experiment.','None','Gothic fiction, Horror fiction, Soft science fiction','Engish','1818','None','Book','frankestein-46'),(47,'The Necklace','2017-06-11 00:00:00','GUY DE MAUPASSANT','Contes et nouvelles','thenecklace',0,0,16,'THE_NECKLACE.txt','Mathilde Loisel is “pretty and charming” but feels she has been born into a family of unfavorable economic status. She was married off to a lowly clerk in the Ministry of Education...','None','Short story','French','1885','None','Story','the-necklace-47'),(48,'The Celebrated Jumping Frog of Calaveras County','2017-06-11 00:00:00','Mark Twain','Unknown','frog1_prr2za',0,0,21,'The_Celebrated_Jumping_Frog_of_Calaveras_County.txt','Story of Jim Smiley and his trained frog. A notorious gambler, Jim was swindled one day when a stranger fed his frog buckshot and made Jim lose a bet.','None','Short story','Engish','1865','None','Story','the-celebrated-jumping-frog-of-calaveras-county-48'),(49,'THE STORY OF AN HOUR','2017-06-12 00:00:00','Kate Chopin','Unknown','51UkRKSeUbL_ucwehj',0,0,17,'THE_STORY_OF_AN_HOUR.txt','Mrs. Mallard, is a married woman with a heart condition. Her husband is away and news comes that ...','None','Short story','Engish','on April 19, 1894','None','Story','the-story-of-an-hour-49'),(50,'An Occurrence at Owl Creek Bridge','2017-06-12 00:00:00','Ambrose Bierce','Unknown','83284ab2a70860109e30194a7c91ffdd_tbbqmu',0,0,20,'An_Occurrence_at_Owl_Creek_Bridge.txt','The man\'s hands were behind his back, the wrists bound with a cord. A rope closely encircled his neck','None','Short story','Engish','13 July 1890','None','Story','an-occurrence-at-owl-creek-bridge-50'),(51,'The Monkey\'s Paw','2017-06-12 00:00:00','W. W. Jacobs','Unknown','monkeys-paw_a5byf3',0,0,17,'The_Monkey\'s_Paw.txt','An old fakir placed a spell on the paw, that it would grant three wishes to three separate men...','None','Supernatural,Fantasy','Engish','September 1902','None','Story','the-monkeys-paw-51'),(52,'The Cask of Amontillado','2017-06-12 00:00:00','Edgar Allan Poe','Godey\'s Lady\'s Book','images_6_wfbkmw',0,0,17,'The_Cask_of_Amontillado.txt','\"The Cask of Amontillado\" has been almost universally referred to as Poe\'s most perfect short story;','None','Horror','Engish','November 1846','None','Story','the-cask-of-amontillado-52'),(53,'The Pit and the Pendulum','2017-06-12 00:00:00','Edgar Allan Poe','Unknown','206172_xt0vdr',0,0,19,'The_Pit_and_the_Pendulum.txt','An unnamed narrator opens the story by revealing that he has been sentenced to death during the time of the Inquisition—an institution of the ...','None','Horror, Tragedy, Gothic fiction, Short story','Engish','September 1839','None','Story','the-pit-and-the-pendulum-53'),(54,'The Fall of the House of Usher','2017-06-12 00:00:00','Edgar Allan Poe','Unknown','images_5_guqa53',0,0,31,'TheFalUsher.txt','In “The Fall of the House of Usher,” a visitor is summoned to the House of Usher by his childhood friend. Usher\'s sister Madeline dies, but somehow comes back ...','None','Horror, Tragedy, Gothic fiction, Short story','Engish','September 1839','None','Story','the-fall-of-the-house-of-usher-54'),(55,'To Build a Fire','2017-06-12 00:00:00','Jack London','Unknown','images_2_m0blfh',0,0,41,'To_Build_a_fire.txt','AY HAD DAWNED COLD AND GRAY WHEN the man turned aside from the main Yukon trail. He climbed the high earth-bank where a ...','None','Adventure, Short story','Engish','29 May 1902','None','Story','to-build-a-fire-55'),(56,'Rip Van Winkle','2017-06-12 00:00:00','Washington_Irving','Unknown','Cover_znymy6',0,0,29,'RipVanWinkle.txt','When he awakens, Van Winkle discovers shocking changes: his musket is rotting and rusty, his beard is a foot long, and his dog is nowhere to be found..','None','Children\'s,Fantasy','Engish','1819','None','Story','rip-van-winkle-56'),(57,'THE CURIOUS CASE OF BENJAMIN BUTTON','2017-06-12 12:00:00','F. Scott Fitzgerald','\"Collier\'s\" last summer','curiouscaseofbenjamin',0,0,56,'THE_CURIOUS_CASE_OF_BENJAMIN_BUTTON.txt','Inspired by a remark of Mark Twain\'s to the effect that it was a pity that the best part of life came at the beginning and the worst part at the end.','None','Fiction','English','27 May 1922','None','Story','the-curious-case-of-benjamin-button-57'),(58,'THE LUCK OF ROARING CAMP','2017-06-15 00:00:00','Bret Harte','Unknown','luckofroaringcamp_rimuma',0,0,24,'THE_LUCK_OF_ROARING_CAMP.txt','“The Luck” is a baby boy born to Cherokee Sal, a fallen woman who dies in childbirth at Roaring Camp, a California gold rush settlement. The men of the camp ...','None','fiction','Engish','August 1868','None','Story','the-luck-of-roaring-camp-58'),(59,'The Yellow Wallpaper.','2017-06-15 00:00:00','Charlotte Perkins Gilman','Unknown','200px-Yellowwp_med_bhksgt',0,0,17,'The_Yellow_Wallpaper.txt','‘The Yellow Wallpaper’ tells the story of a woman who suffers from “temporary nervous depression—a slight hysterical tendency”, and whose husband, John,..','None','Short story, Women\'s health, Mental disorder','Engish','January 1892','None','Story','the-yellow-wallpaper-59'),(60,'The Masque of the Red Death','2017-06-15 00:00:00','Gothic fiction','Unknown','Punchdrunk_Masque_of_the_Red_Death_promo_image_su7u2m',0,0,16,'The_Masque_of_the_Red_Death.txt','The \"Red Death\" had long devastated the country. No pestilence had ever been so fatal, or so hideous. Blood was its Avatar and its seal—the rednes..','None','Gothic fiction','Engish','May 1842','None','Story','the-masque-of-the-red-death-60'),(61,'The Purloined Letter','2017-05-06 00:00:00','Edgar Allan Poe','Unknown','thepurloinedletter_w0riix',0,0,22,'The_Purloined_Letter.txt','In a small room in Paris, an unnamed narrator, who also narrates “The Murders in the Rue Morgue,” sits quietly with his friend, C. Auguste Dupin','None','Detective fiction, Short story','Engish','December 1844','None','Story','the-purloined-letter-61'),(62,'Alice’s Adventures in Wonderland','2017-07-07 00:00:00','Lewis Carroll','Unknown','Alice-s-Adventures-in-Wonderland_iqcb07',0,0,82,'aliceinwonderland.txt','Alice was beginning to get very tired of sitting by her sister on the bank, and of having nothing to do: once or twice','None','Fantasy,ShortStory,Fiction','English','March, 1994','None','Book','alices-adventures-in-wonderland-62'),(63,'A Tale Of two cities','2017-07-30 00:00:00','Charles Dickens','A Story of the French Revolution','taleoftwocities_jpisd1',0,0,16,'A_Tale_Of_two_cities.txt','There were a king with a large jaw and a queen with a plain face, on the throne of England; there were a king with a large jaw and a queen with a fair face, on the throne of France. In both countries it was clearer than crystal to the lords of','No','fiction','Engish','January, 1994','None','Book','a-tale-of-two-cities-63'),(64,'The Happy Prince','2017-07-30 00:00:00','Oscar Wilde','Unknown','51z6Yf_2BIBOL._AC_UL320_SR248_320__dehm3r',0,0,87,'The_Happy_Prince.txt','High above the city, on a tall column, stood the statue of the Happy Prince.  He was gilded all over with thin leaves of fine gold, for eyes he had two bright sapphires, and a large red ruby glowed on his sword-hilt.','None','fiction','Engish','None','None','Story','the-happy-prince-64'),(65,'The Selfish Giant','2017-05-06 00:00:00','Oscar Wilde','The Happy Prince   and Other Tales','TheSelfishGiant_Cover',0,0,100,'The_Selfish_Giant.txt','It was a large lovely garden, with soft green grass.  Here and there over the grass stood beautiful flowers like stars, and there were twelve peach-trees that in the spring-time broke out into delicate blossoms of pink and pearl, and...','None','Children\'s,Fantasy,Fairytale','Engish','None','None','Story','the-selfish-giant-65'),(66,'The Remarkable Rocket','2017-05-06 00:00:00','Oscar Wilde','The Happy Prince   and Other Tales','REMARKABLE_ROCKET_Cover',0,0,18,'THE_REMARKABLE_ROCKET.txt','THE REMARKABLE ROCKET, by Oscar Wilde. THE King\'s son was going to be married, so there were general rejoicings. He had waited a whole year for his ...','None','Children\'s,Fantasy,Fairytale','Engish','1888','None','Story','the-remarkable-rocket-66'),(67,'Poos ki Raat','2017-05-06 00:00:00','MUNSHI PREMCHAND','Unknown','pooskiraat_cover',0,0,65,'Poos_Ki_Raat.txt','mid-January. The nights are naturally cold. Here the author explores a single day in the life of a farmer, who is deep in debt and has to pay taxes for his land. There is a crop waiting to be harvested, and he must keep vigil at night to prevent anim','None','Short story','Hindi','None','None','Story','poos-ki-raat-67'),(68,'A CHRISTMAS CAROL','2017-05-06 00:00:00','Charles Dickens','Unknown','christmascarol_cover',0,0,340,'A_CHRISTMAS_CAROL.txt','Marley was dead: to begin with. There is no doubt whatever about that. The register of his burial was signed by the clergyman, the clerk, the undertaker, and the chief mourner.','None','Short story','Engish','19 December 1843','None','Book','a-christmas-carol-68'),(69,'Idgah','2017-09-02 00:00:00','Munshi Premchand','Unknown','idgah_uh7hq7',0,0,75,'idgah.txt','Idgah tells the story of a four-year-old orphan named Hamid who lives with his grandmother Amina. Hamid, the protagonist of the story, has recently lost his parents; however his grandmother tells him that his father has left to earn mone','None','fiction','Hindi','1938','None','Story','idgah-69');
/*!40000 ALTER TABLE `personal_story` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `personal_uservotes`
--

DROP TABLE IF EXISTS `personal_uservotes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `personal_uservotes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vote_type` varchar(200) NOT NULL,
  `post_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `personal_uservotes_user_id_6fd1c5d3_uniq` (`user_id`,`post_id`,`vote_type`),
  KEY `personal_uservotes_post_id_271cbcf8_fk_personal_story_id` (`post_id`),
  CONSTRAINT `personal_uservotes_post_id_271cbcf8_fk_personal_story_id` FOREIGN KEY (`post_id`) REFERENCES `personal_story` (`id`),
  CONSTRAINT `personal_uservotes_user_id_996e1cfa_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `personal_uservotes`
--

LOCK TABLES `personal_uservotes` WRITE;
/*!40000 ALTER TABLE `personal_uservotes` DISABLE KEYS */;
/*!40000 ALTER TABLE `personal_uservotes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `social_auth_association`
--

DROP TABLE IF EXISTS `social_auth_association`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `social_auth_association` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `server_url` varchar(255) NOT NULL,
  `handle` varchar(255) NOT NULL,
  `secret` varchar(255) NOT NULL,
  `issued` int(11) NOT NULL,
  `lifetime` int(11) NOT NULL,
  `assoc_type` varchar(64) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `social_auth_association_server_url_078befa2_uniq` (`server_url`,`handle`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `social_auth_association`
--

LOCK TABLES `social_auth_association` WRITE;
/*!40000 ALTER TABLE `social_auth_association` DISABLE KEYS */;
/*!40000 ALTER TABLE `social_auth_association` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `social_auth_code`
--

DROP TABLE IF EXISTS `social_auth_code`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `social_auth_code` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(254) NOT NULL,
  `code` varchar(32) NOT NULL,
  `verified` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `social_auth_code_email_801b2d02_uniq` (`email`,`code`),
  KEY `social_auth_code_c1336794` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `social_auth_code`
--

LOCK TABLES `social_auth_code` WRITE;
/*!40000 ALTER TABLE `social_auth_code` DISABLE KEYS */;
/*!40000 ALTER TABLE `social_auth_code` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `social_auth_nonce`
--

DROP TABLE IF EXISTS `social_auth_nonce`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `social_auth_nonce` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `server_url` varchar(255) NOT NULL,
  `timestamp` int(11) NOT NULL,
  `salt` varchar(65) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `social_auth_nonce_server_url_f6284463_uniq` (`server_url`,`timestamp`,`salt`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `social_auth_nonce`
--

LOCK TABLES `social_auth_nonce` WRITE;
/*!40000 ALTER TABLE `social_auth_nonce` DISABLE KEYS */;
/*!40000 ALTER TABLE `social_auth_nonce` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `social_auth_partial`
--

DROP TABLE IF EXISTS `social_auth_partial`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `social_auth_partial` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token` varchar(32) NOT NULL,
  `next_step` smallint(5) unsigned NOT NULL,
  `backend` varchar(32) NOT NULL,
  `data` longtext NOT NULL,
  PRIMARY KEY (`id`),
  KEY `social_auth_partial_94a08da1` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `social_auth_partial`
--

LOCK TABLES `social_auth_partial` WRITE;
/*!40000 ALTER TABLE `social_auth_partial` DISABLE KEYS */;
/*!40000 ALTER TABLE `social_auth_partial` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `social_auth_usersocialauth`
--

DROP TABLE IF EXISTS `social_auth_usersocialauth`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `social_auth_usersocialauth` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `provider` varchar(32) NOT NULL,
  `uid` varchar(255) NOT NULL,
  `extra_data` longtext NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `social_auth_usersocialauth_provider_e6b5e668_uniq` (`provider`,`uid`),
  KEY `social_auth_usersocialauth_user_id_17d28448_fk_auth_user_id` (`user_id`),
  CONSTRAINT `social_auth_usersocialauth_user_id_17d28448_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `social_auth_usersocialauth`
--

LOCK TABLES `social_auth_usersocialauth` WRITE;
/*!40000 ALTER TABLE `social_auth_usersocialauth` DISABLE KEYS */;
INSERT INTO `social_auth_usersocialauth` VALUES (1,'facebook','10211513025875366','{\"id\": \"10211513025875366\", \"access_token\": \"EAADfh6DfgasBAEOz2eWLLAa86wdgFjSuZBbf7ZCnUGKt4ra2vTAz7JmMWsk9rd2FqYsrr10d92TmHgYnzFlNE6ivXQDsR5BYkV9XQq3ppKeYLeAvcXnEkbEIZAv2FUiCnfQtLb0eH5OD3wW4K2MTDASfHfxB3YZD\", \"expires\": 5183999, \"denied_scopes\": null, \"token_type\": null, \"auth_time\": 1503812795, \"granted_scopes\": [\"public_profile\"]}',1),(2,'facebook','106899146602896','{\"id\": \"106899146602896\", \"auth_time\": 1498379607, \"denied_scopes\": null, \"granted_scopes\": [\"public_profile\"], \"access_token\": \"EAADfh6DfgasBAAxg8i30E27bMbt0FinZCzeSsUFyefhLWKTbllxZAwyfU7ZBfjvrJkAhYKi5BSzK8VDbarxvLgwEZCgcD1FGJ2ITqZCrY4K6ZABiWZAuOU3wCjqBEu2xIwZAZAQCn83EXB5mzH9UJvqZCwrF79EJpQUevO3pZCBIT8puTrRZCnXFAerL\", \"expires\": 5184000, \"token_type\": null}',2),(3,'facebook','1525236760861387','{\"id\": \"1525236760861387\", \"access_token\": \"EAADfh6DfgasBAJWJpaOuaGYMivYCiiYblZAsbsVogPBdtXNBvXm4Rd74pYQIXm6cH8K8Ge4sTEombznDBG1gfApnqMN5ulcvQssGeGogxGO3c9HftWZBkxUwwxjAaJSVrdod6R46FSrZCQEZAemSKsncakcoKaZCN6ipbeNLjdgZDZD\", \"expires\": 5184000, \"denied_scopes\": null, \"token_type\": null, \"auth_time\": 1503919521, \"granted_scopes\": [\"public_profile\"]}',3),(4,'facebook','10207628662514001','{\"auth_time\": 1498467424, \"denied_scopes\": null, \"id\": \"10207628662514001\", \"granted_scopes\": [\"public_profile\"], \"token_type\": null, \"expires\": 5183999, \"access_token\": \"EAADfh6DfgasBAIjSBl1doWqxGwu00IkE8iZCujzBeortU9ITbMt8UAL6eWn78ZAAsNNXK5iVe9tmxIFcGpEIT06djTFjY7FgKE6TXZA5oqSeK4whFTaJTrFuh7b6vPemefo0Tgbj5zRh4QZBAU7co9hc74mlZC4sZD\"}',4),(5,'facebook','1458288337583589','{\"id\": \"1458288337583589\", \"token_type\": null, \"access_token\": \"EAADfh6DfgasBAG0dqKnhIahR8nrsXqaISmDQTbHHE0DVi3WVor1deC2K7nQEVV6QO1beplq8nJ7oTPHnzsK0LL6XvwbCtSON6Y6JyZByhMXZA6ZBpwpGcBFWsNaJ3s586xj4NK29JllApaBUA2F0OBHbJ5WaGz9LuEMzNVWAAZDZD\", \"expires\": 5183999, \"auth_time\": 1503818867, \"granted_scopes\": [\"public_profile\"], \"denied_scopes\": null}',5);
/*!40000 ALTER TABLE `social_auth_usersocialauth` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-09-06 19:33:54
